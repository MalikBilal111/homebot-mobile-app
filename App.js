import React from "react";
//Redux
import { Provider } from "react-redux";
import store from "./src/redux/configureStore";
// Theme Setup
import theme from "./src/styles/theme";
import { Provider as PaperProvider } from "react-native-paper";
// Navigation Setup
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
//Screens
import LoginUser from "./src/screens/login/index"
import RegisterScreen from "./src/screens/register/index";
import VerficationScreen from "./src/screens/onboarding/verification";
import FingerPrintSetup from "./src/screens/onboarding/fingerprintsetup";
import HomeSetup from "./src/screens/onboarding/homesetup";
import WelcomeScreen from "./src/screens/onboarding/welcome";
import DashboardScreen from "./src/screens/dashboard/drawer";
import bottomnavigation from "./src/screens/dashboard/bottomnavigation";
const Stack = createNativeStackNavigator();
export default function App() {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="LoginUser" screenOptions={{ headerShown: false }}>
            <Stack.Screen name="LoginUser" component={LoginUser} />
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
            <Stack.Screen name="VerficationScreen" component={VerficationScreen} />
            <Stack.Screen name="FingerPrintSetup" component={FingerPrintSetup} />
            <Stack.Screen name="HomeSetup" component={HomeSetup} />
            <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} />
            <Stack.Screen name="DashboardScreen" component={bottomnavigation} />
          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>
    </Provider>
  );
}

