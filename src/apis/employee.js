import queryString from "query-string";
import { axios } from "../utils/axios";
import { getErrors } from "../utils/validations";
import moment from "moment";

export const uploadDocuments = (data) => {
  let result = axios
    .post("employee/employeeDocument", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          data: res.data.data,
          status: "success",
          message: "Documents uploaded successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Documents upload failed! ",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Documents upload failed! " };
    });
  return result;
};

export const withdrawResignation = (id) => {
  let result = axios
    .delete(`employee/resignation/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Resignation request sent successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        message: "Resignation request withdrawn failed",
      };
    });
  return result;
};

export const applyResign = (data) => {
  // type can be all, read, unread
  // make it handle
  let result = axios
    .post(`employee/resignation`, data)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { result: {}, status: "failure" };
    });
  return result;
};

export const downloadPayslip = (transaction_id) => {
  let result = axios
    .get(`employee/payslip/${transaction_id}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { result: {}, status: "failure" };
    });
  return result;
};

export const getDashboardData = (__) => {
  return axios.get("employee/dashboard").then((result) => {
    return result.data;
  });
};

export const getRecentInterviews = (__) => {
  return axios.get("employee/recentInterview").then((result) => {
    return result.data;
  });
};

export const getRecentJobOffers = (__) => {
  return axios.get("employee/latestJobOffers").then((result) => {
    return result.data;
  });
};

export const getRecentAttendance = (__) => {
  return axios.get("employee/employeesAttendance").then((result) => {
    return result.data;
  });
};

export const getRecentLeavesReport = (__) => {
  return axios.get("employee/employeesLeaves").then((result) => {
    return result.data;
  });
};

export const getGoals = (__) => {
  return axios.get("employee/employeesLeaves").then((result) => {
    let temp = {};
    temp.data = [];
    return temp;
  });
};

export const getAnnouncements = (__) => {
  return axios.get("employee/employeesLeaves").then((result) => {
    let temp = {};
    temp.data = [];
    return temp;
  });
};

export const getPayrollRules = () => {
  // type can be all, read, unread
  // make it handle
  let data = axios
    .get(`employee/payroll-rules`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const unreadNotifications = (notifications) => {
  // type can be all, read, unread
  // make it handle
  let data = axios
    .post(`employee/delete-notifications`, { ids: notifications })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const readNotifications = (notifications) => {
  // type can be all, read, unread
  // make it handle
  let data = axios
    .post(`employee/read-notifications`, { ids: notifications })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const clearNotifications = (notifications) => {
  // type can be all, read, unread
  // make it handle
  let data = axios
    .post(`employee/delete-notifications`, { ids: notifications })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const getNotifications = (type = "all", params) => {
  let query = queryString.stringify(params);

  let data = axios
    .get(`employee/notifications/${type}?${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const getFiltersData = () => {
  let data = axios
    .get(`employee/filter-data`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { err };
    });
  return data;
};

export const deleteLeave = (delete_id) => {
  let result = axios
    .delete(`employee/leave-list/${delete_id}`)
    .then((res) => {
      if (res.data.status === "success") return res.data;
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};

export const updateLeave = (data, leave_id) => {
  let result = axios
    .patch(`employee/apply-leave/${leave_id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Leave application sent successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Leave request failed!" };
    });
  return result;
};

export const applyLeave = (data) => {
  let result = axios
    .post("employee/apply-leave", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          data: res.data.data,
          status: "success",
          message: "Leave applied successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Some issue occured!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Leave application failed!" };
    });
  return result;
};

export const getLeaves = (id, query) => {
  let data = axios
    .get(`employee/leave-list/${id}${query}`)
    .then((res) => {
      let jobs = res.data;
      return jobs;
    })
    .catch((err) => {
      return { err };
    });
  return data;
};

export const getMyJobDetail = (id) => {
  let data = axios
    .get(`employee/employee-jobs/${id}`)
    .then((res) => {
      let jobs = res.data;
      return jobs;
    })
    .catch((err) => {
      return { err };
    });
  return data;
};

export const getMyJobs = (query = "") => {
  let data = axios
    .get(`employee/employee-jobs${query}`)
    .then((res) => {
      let jobs = res.data;
      return jobs;
    })
    .catch((err) => {
      return { err };
    });
  return data;
};

export const getTransactions = (query = "") => {
  let data = axios
    .get(`employee/employee-wallet-transactions${query}`)
    .then((res) => {
      let bank = res.data;
      return bank;
    })
    .catch((err) => {
      //
      return { data: null };
    });
  return data;
};

// employee bank account stuff start
export const editBankInfo = (data) => {
  let result = axios
    .post("employee/update-bank-account", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          data: res.data.data,
          status: "success",
          message: "Bank account details updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message,
        };
    })
    .catch((err) => {
      return {
        status: "error",
        message: "Bank account details update failed!",
      };
    });
  return result;
};
export const addBankInfo = (data) => {
  let result = axios
    .post("employee/add-bank-account", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          data: res.data.data,
          status: "success",
          message: "Bank account details added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message,
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Adding Bank account details failed!",
      };
    });
  return result;
};

export const getAndValidateBankInfo = () => {
  let data = axios
    .get(`employee/validate-bank-account`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: null };
    });
  return data;
};
export const withdrawFunds = (data) => {
  let result = axios
    .post("employee/employeeWithdrawDeposit", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Funds withdrawal requested successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ? res.data.message : "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Withdrawal request failed!" };
    });

  return result;
};
// employee bank account stuff end

export const deleteItem = (item, id) => {
  let result = axios({
    method: "delete",
    url: `employee/${item}/0`,
    data: { ids: [id] },
  })
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: `${item} removed successfully`,
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};

export const createDispute = (data) => {
  let result = axios
    .post("employee/attendanceDispute", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          data: res.data.data,
          status: "success",
          message: "Dispute submitted successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Dispute submission failed!" };
    });
  return result;
};

export const getAttendance = (employee_job_id, query = "") => {
  let data = axios
    .get(`employee/timesheet/${employee_job_id}${query}`)
    .then((res) => {
      let timesheet = res.data;
      return timesheet;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};
// get info like balance, checkin/checkouts, job id
export const getEmployeeInfo = () => {
  let data = axios
    .get(`employee/info`)
    .then((res) => {
      let info = res.data;
      return info;
    })
    .catch((err) => {
      //
      return { data: [], error: "Error occured!" };
    });
  return data;
};

export const checkIn = (data) => {
  let result = axios
    .post("employee/jobAttendance", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          data: res.data.data,
          status: "success",
          message: "You have Checked in successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Your check-in failed!" };
    });
  return result;
};

export const checkOut = (attendance_id, data) => {
  let result = axios
    .patch(`employee/jobAttendance/${attendance_id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "You have checked out successfully!",
          data: res.data.data,
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Your checkout failed!" };
    });
  return result;
};

// APIS FOR JOB
export const getEmployeeSettings = () => {
  let data = axios
    .get(`employee/appSettings`, {
      params: { keys: ["service_fee", "min_balance"], type: "employee" },
    })
    .then((res) => {
      let employees = res.data;
      return employees;
    })
    .catch((err) => {
      return { data: [], error: "Error occured!" };
    });
  return data;
};

export const getAppSettings = () => {
  let data = axios
    .get(`employee/appSettings`, {
      params: {
        keys: ["interview_start_time", "interview_end_time"],
        type: "app",
      },
    })
    .then((res) => {
      let employees = res.data;
      return employees;
    })
    .catch((err) => {
      return { data: [], error: "Error occured!" };
    });
  return data;
};

// export const acceptInterview = (data) => {
//   let result = axios
//   .post('employee/update-interview', data)
//   .then((res) => {
//     if (res.data.status === "success")
//       return {
//         status: "success",
//         message: "Interview accepted",
//       };
//     else
//       return {
//         status: "error",
//         errors: res.data.errors ? getErrors(res.data.errors) : null,
//         message: "Kindly fix the data",
//       };
//   })
//   .catch((err) => {
//
//     return { status: "error", message: "Interview schedule failed " };
//   });

// return result;
// }

export const rescheduleInterview = (data) => {
  let result = axios
    .post("employee/reschedule-interview", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Interview reschedule request sent successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        message: "Interview reschedule request failed!",
      };
    });
  return result;
};

export const getInterviews = (interviewsStatus, query = "") => {
  let queryParams = queryString.parse(query);
  queryParams.status = interviewsStatus;
  let querystring = queryString.stringify(queryParams, {
    arrayFormat: "bracket",
  });

  let data = axios
    .get(`employee/employee-interviews?${querystring}`)
    .then((res) => {
      let interviews = res.data;
      return interviews;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getJobOffers = (query = "") => {
  //
  let data = axios
    .get(`employee/jobOffers${query}`)
    .then((res) => {
      let offers = res.data;
      return offers;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getJobOffersList = (query = "") => {
  let data = axios
    .get(`employee/jobOffersList${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [] };
    });
  return data;
};

export const counterOffer = (job_id, status, data) => {
  let result = axios
    .get(`employee/jobOffers/${job_id}/${status}`, {
      params: { ...data },
    })
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Job offer updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Job offer update failed!" };
    });

  return result;
};

export const updateJobOffer = (job_id, status) => {
  let result = axios
    .get(`employee/jobOffers/${job_id}/${status}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Job offer updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Job offer update failed!" };
    });

  return result;
};

export const applyForJob = (data) => {
  let result = axios
    .post("employee/employee-jobs", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Job application sent successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Job application failed!" };
    });

  return result;
};

export const getJobsById = (id) => {
  let data = axios
    .get(`employee/job-detail/${id}`)
    .then((res) => {
      let jobs = res.data.data;
      return jobs;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getMyJobsById = (id) => {
  let data = axios
    .get(`employee/my-job-detail/${id}`)
    .then((res) => {
      let jobs = res.data.data;
      return jobs;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getJobs = ($query) => {
  let data = axios
    .get(`employee/matched-jobs${$query}`)
    .then((res) => {
      let jobs = res.data;
      return jobs;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

// APIS FOR EMPLOYEE SCREEN START
export const getProfile = () => {
  let data = axios
    .get(`employee/jobProfile`)
    .then((res) => {
      let profile = res.data;
      return profile;
    })
    .catch((err) => {
      //
      return { status: "error", message: err };
    });
  return data;
};

// update employee account basic info
export const editEmployeeInformation = (data, id, role = "employee") => {
  let path = role === "employee" ? "employee/profile" : "admin/employees";
  let result = axios
    .patch(`${path}/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Profile updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Profile update failed!" };
    });

  return result;
};

// it's the resume
export const createJobProfile = (data) => {
  let result = axios
    .post("employee/jobProfile", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Job profile created successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Adding job profile failed! " };
    });
  return result;
};
export const editJobProfile = (data, id) => {
  let result = axios
    .patch(`employee/jobProfile/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Job profile updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Job profile update failed!" };
    });
  return result;
};

export const createExperience = (data, id, role = "employee") => {
  let path =
    role === "employee" ? "employee/jobExperience" : "admin/employeeExperience";
  let result = axios
    .post(path, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Experience added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Adding experience failed!" };
    });
  return result;
};

export const editExperience = (data, id, role = "employee") => {
  let path =
    role === "employee" ? "employee/jobExperience" : "admin/employeeExperience";

  let result = axios
    .patch(`${path}/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Experience updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Experience update failed!" };
    });

  return result;
};
export const deleteExperience = (id) => {
  let result = axios
    .delete(`employee/jobExperience/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Experience removed successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};
export const createEducation = (data, id) => {
  let result = axios
    .post("employee/employeeEducation", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Education added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Adding Education failed!" };
    });
  return result;
};
export const deleteEducation = (id) => {
  let result = axios
    .delete(`employee/employeeEducation/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Education removed successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};
export const editEducation = (data, id) => {
  let result = axios
    .patch(`employee/employeeEducation/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Education updated successfully",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Education update failed!" };
    });

  return result;
};

export const createLanguage = (data, id) => {
  let url = `employee/employeeLanguage`;
  let result = axios
    .post(url, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Language added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Language add failed!" };
    });
  return result;
};

export const deleteLanguage = (id) => {
  let result = axios
    .delete(`employee/employeeLanguage/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Language removed successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};
export const editLanguage = (data, id) => {
  let result = axios
    .patch(`employee/employeeLanguage/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Language update successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Language update failed!" };
    });

  return result;
};

export const createSkills = (data) => {
  let result = axios
    .post("employee/jobSkills", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Skills update successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Skills update failed!" };
    });
  return result;
};

export const updateSkills = (data) => {
  let result = axios
    .patch("employee/jobSkills/1", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Skills updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Skills update failed!" };
    });
  return result;
};

// export const createEditSkills = (data, id) => {
//   let url = id? `employee/jobSkills/${id}`:`employee/jobSkills`
//   let result = axios
//     .post(url, data)
//     .then((res) => {
//       if (res.data.status === "success")
//         return {
//           status: "success",
//           data: res.data.data,
//           message: "Skills updated successfully",
//         };
//       else
//         return {
//           status: "error",
//           errors: res.data.errors ? getErrors(res.data.errors) : null,

//           message: "Kindly fix the data",
//         };
//     })
//     .catch((err) => {
//       return { status: "error", message: "Skills updates failed " };
//     });
//   return result;
// }

export const createEditPortfolio = (data, id) => {
  let url = id ? `employee/jobPortfolio/${id}` : `employee/jobPortfolio`;
  let result = axios
    .post(url, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Portfolio updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Portfolio update failed!" };
    });
  return result;
};
export const createPortfolio = (data, id) => {
  let result = axios
    .post("employee/jobPortfolio", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Portfolio added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: res.data.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Adding portfolio failed!" };
    });
  return result;
};

export const editPortfolio = (data, id) => {
  let result = axios
    .patch(`employee/jobPortfolio/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          message: "Portfolio updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Portfolio update failed!" };
    });

  return result;
};
export const deletePortfolio = (id) => {
  let result = axios
    .delete(`employee/jobPortfolio/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Portfolio removed successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};

export const resendVerification = () => {
  return axios.get("employee/resend-verification-email").then((result) => {
    return result.data;
  });
};
// APIS FOR EMPLOYEE SCREEN END

const COUNTRIES_DATA = [
  {
    id: 2,
    title: "Pakistan",
    sortName: "AF",
    phoneCode: "+102",
    currency: "AR",
    dateCreated: "1 month ago",
  },
  {
    id: 4,
    title: "China",
    sortName: "AF",
    phoneCode: "+104",
    currency: "AR",
    dateCreated: "1 month ago",
  },
];

const STATES_DATA = [
  { id: 1, title: "Punjab", countryID: 2 },
  { id: 2, title: "KPK", countryID: 2 },
  { id: 3, title: "Sindh", countryID: 2 },
  { id: 4, title: "Balochistan", countryID: 2 },
  { id: 5, title: "Xin Shun", countryID: 4 },
  { id: 6, title: "Ki Ju", countryID: 4 },
];

const CITIES_DATA = [
  { id: 1, title: "Islamabad", stateID: 1 },
  { id: 2, title: "Lahore", stateID: 1 },
  { id: 3, title: "Khanewal", stateID: 1 },
  { id: 4, title: "Multan", stateID: 1 },
  { id: 5, title: "Peshawar", stateID: 2 },
  { id: 6, title: "Karachi", stateID: 3 },
  { id: 7, title: "Quetta", stateID: 4 },
  { id: 8, title: "Shanghai", stateID: 5 },
  { id: 9, title: "Hongkong", stateID: 6 },
];

// COUNTRIES
export const getAllCountries = () => {
  return COUNTRIES_DATA;
};
export const getCountryByID = (id) => {
  return COUNTRIES_DATA.filter((item) => item.id === id)[0];
};

export const getStatesByCountryID = (id) => {
  return STATES_DATA.filter((item) => item.countryID === id);
};

export const getStateByID = (id) => {
  return STATES_DATA.filter((item) => item.id === id)[0];
};

export const getCitiesByStateID = (id) => {
  return CITIES_DATA.filter((item) => item.stateID === id);
};

export const getCityById = (id) => {
  return CITIES_DATA.filter((item) => item.id === id)[0];
};

export const getCityDetails = (id) => {
  let city = getCityById(id);
  city.state = getStateByID(city.stateID);
  city.country = getCountryByID(city.state.countryID);
  return city;
};

export const getCityDetailsByName = (name) => {
  let city = CITIES_DATA.filter((item) => item.title === name)[0];
  city.state = getStateByID(city.stateID);
  city.country = getCountryByID(city.state.countryID);
  return city;
};

export const getUser = (id) => {
  return {
    firstName: "Jonathan",
    lastName: "McGrath",
    headline: "Your Creative Partner - Branding Specialist - CEO",
    email: "aimegratg@gmail.com",
    phone: "(+92)310-4131234",
    gender: "Male", //countryID: 1, stateID: 1, cityID: 1
    //cnic: '',
    dateOfBirth: "9/9/1994",
    experiences: [
      {
        title: "Vistage Amit | Brand Manager",
        employementType: "Fulltime", // subtitle: 'Your Creative Partner - Branding Specialist - SEO Master',
        jobType: "?",
        company: "React First",
        startDate: "1/1/2018",
        endDate: "1/4/2019",
        companySector: "Tech",
        industry: "Tech?",
        city: 1, // countryID: 1, country can be accessed from city
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      },
    ],
    education: [
      {
        title: "Master Computer Science",
        institute: "University of California",
        date: "14 April 2014",
      },
    ],
  };
};

const INTERVIEWS = [
  {
    id: 1,
    jobId: 0,

    jobTitle: "Content Writer",
    jobExperience: "Less than 1 year",
    hourlyBudget: 12,
    scheduledDateTime: moment().format("DD/MM/yyyy"),
    status: "scheduled",
  },
  {
    id: 2,
    jobId: 1,

    jobTitle: "Sports Content Writer",
    jobExperience: "Less than 1 year",
    hourlyBudget: 12,
    scheduledDateTime: moment().format("DD/MM/yyyy"),
    status: "scheduled",
  },
  {
    id: 3,
    jobId: 2,
    jobTitle: "Corporate Content Writer",
    jobExperience: "Less than 1 year",
    hourlyBudget: 12,
    scheduledDateTime: moment().format("DD/MM/yyyy"),
    status: "completed",
  },
];

const TRANSACTIONS = [
  {
    date: moment(),
    // name: "Asad Jakhar",
    amount: 100,
    hours: 4,
    type: "credit",
    status: "added",
    transactionTime: moment(),
  },
];

const LEAVES = [
  {
    id: 0,
    employee: "Asad Jakhar",
    date: moment(),
    dateCreated: moment(),
    leaveType: "Sick Leaves",
    dateUpdated: moment(),
    status: "pending",
    description:
      "Ea irure nisi nulla fugiat amet veniam. Amet irure aliquip nostrud laborum dol or duis minim pariatur labore aute adipisicing commodo anim nulla. Amet ipsum voluptate esse eiusmod laborum incididunt sit nisi consectetur do id proident et deserunt. Deserunt consectetur officia id voluptate adipisicing aliqua magna elit irure. Adipisicing proident veniam quis culpa dolore. Deserunt magna duis commodo nulla deserunt exercitation amet magna esse. Sunt amet dolore minim consectetur minim est Lorem minim consectetur amet.",
  },
  {
    id: 1,
    employee: "Asad Jakhar",

    date: moment(),
    dateCreated: moment(),
    leaveType: "Sick Leaves",
    dateUpdated: moment(),
    status: "rejected",
    description:
      "Ea irure nisi nulla fugiat amet veniam. Amet irure aliquip nostrud laborum dol or duis minim pariatur labore aute adipisicing commodo anim nulla. Amet ipsum voluptate esse eiusmod laborum incididunt sit nisi consectetur do id proident et deserunt. Deserunt consectetur officia id voluptate adipisicing aliqua magna elit irure. Adipisicing proident veniam quis culpa dolore. Deserunt magna duis commodo nulla deserunt exercitation amet magna esse. Sunt amet dolore minim consectetur minim est Lorem minim consectetur amet.",
  },
  {
    id: 2,
    employee: "Asad Jakhar",

    date: moment(),
    dateCreated: moment(),
    leaveType: "Random Leaves",
    dateUpdated: moment(),
    status: "approved",
    description:
      "Ea irure nisi nulla fugiat amet veniam. Amet irure aliquip nostrud laborum dol or duis minim pariatur labore aute adipisicing commodo anim nulla. Amet ipsum voluptate esse eiusmod laborum incididunt sit nisi consectetur do id proident et deserunt. Deserunt consectetur officia id voluptate adipisicing aliqua magna elit irure. Adipisicing proident veniam quis culpa dolore. Deserunt magna duis commodo nulla deserunt exercitation amet magna esse. Sunt amet dolore minim consectetur minim est Lorem minim consectetur amet.",
  },
  {
    id: 3,
    employee: "Asad Jakhar",

    date: moment(),
    dateCreated: moment(),
    leaveType: "Bonus Leaves",
    dateUpdated: moment(),
    status: "pending",
    description:
      "Ea irure nisi nulla fugiat amet veniam. Amet irure aliquip nostrud laborum dol or duis minim pariatur labore aute adipisicing commodo anim nulla. Amet ipsum voluptate esse eiusmod laborum incididunt sit nisi consectetur do id proident et deserunt. Deserunt consectetur officia id voluptate adipisicing aliqua magna elit irure. Adipisicing proident veniam quis culpa dolore. Deserunt magna duis commodo nulla deserunt exercitation amet magna esse. Sunt amet dolore minim consectetur minim est Lorem minim consectetur amet.",
  },
];

const TIMESHEET = [
  {
    id: 0,
    checkin: moment(),
    checkout: moment(),
    hours: 4.0,
    rate: 6,
    serviceFeePercent: 25,
    isDisputed: false,
  },
  {
    id: 1,
    checkin: moment(),
    checkout: moment(),
    hours: 6.0,
    rate: 10,
    serviceFeePercent: 25,
    isDisputed: true,
  },
  {
    id: 2,
    checkin: moment(),
    checkout: moment(),
    hours: 8.0,
    rate: 4,
    serviceFeePercent: 50,
    isDisputed: true,
  },
];
