
import moment from "moment";

import { axios } from "../utils/axios";
import { getErrors } from "../utils/validations";

export const uploadFiles = (files, progress_tracker_cb) => {


  let result = axios
    .post(`upload`, files, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: progressEvent => {
        if (progress_tracker_cb)
          progress_tracker_cb(progressEvent)
      }
    })
    .then((res) => {
       
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Files uploaded successfully",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Issue occured while uploading",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Files upload failed (Max: 2mb)" };
    });
  return result;

}