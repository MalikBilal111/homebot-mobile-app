import queryString from "query-string";
import { axios } from "../utils/axios";
import { getErrors } from "../utils/validations";

export const getAllClosedReasons = () => {
  let data = axios
    .get(`employer/closed-reasons`)
    .then((res) => {
      return res.data.data;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getAllTransactionCategories = () => {
  let data = axios
    .get(`transactioncategoriesall`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

//
// API to create
export const requestRefund = (data) => {
  let result = axios
    .post(`employer/refundRequest`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Refund requested successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Refund request failed!" };
    });
  return result;
};

export const addShiftTiming = (data) => {
  let result = axios
    .post(`employer/add-shift`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Shift added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data?.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Some issue occured" };
    });
  return result;
};

export const sendMoney = (data) => {
  let result = axios
    .post(`employer/send-funds`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Money sent successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data?.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Some issue occured" };
    });
  return result;
};

// API to create
export const employeeTerminate = (data) => {
  let result = axios
    .post(`employer/employeeTerminate`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Employee termination requested successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Termination request failed!" };
    });
  return result;
};

export const employeeWithdrawTerminate = (id) => {
  let result = axios
    .delete(`employer/employeeTerminate/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Employee termination request withdrawn successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Termination withdrawn failed!" };
    });
  return result;
};

// team members start
export const getTeamMembers = (id) => {
  // let query = queryString.stringify(params)

  let data = axios
    .get(`employer/employeeTeam/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const addTeamMember = (data) => {
  let result = axios
    .post(`employer/employeeTeam`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Team member added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Adding Team member failed!" };
    });
  return result;
};

export const editTeamMember = (data, id) => {
  let result = axios
    .patch(`employer/employeeTeam/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Team member updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Kindly fix the data",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Team member update failed!",
      };
    });
  return result;
};
export const deleteTeamMember = (id) => {
  let result = axios
    .delete(`employer/employeeTeam/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Team member removed successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};
// /Team members end

// teams start
export const getTeams = () => {
  // let query = queryString.stringify(params)

  let data = axios
    .get(`employer/teams`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const addTeam = (data) => {
  let result = axios
    .post(`employer/teams`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Team added successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: "Kindly fix the data",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Adding team failed!" };
    });
  return result;
};

export const editTeam = (data, id) => {
  let result = axios
    .patch(`employer/teams/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Team updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", errors: {}, message: "Team update failed!" };
    });
  return result;
};
export const deleteTeam = (id) => {
  let result = axios
    .delete(`employer/teams/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Team deleted successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while removing!",
      };
    });
  return result;
};
// /Teams end

export const getEstimatedFunds = () => {
  // let query = queryString.stringify(params)

  let data = axios
    .get(`employer/estimated-funds`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const unreadNotifications = (notifications) => {
  // type can be all, read, unread
  // make it handle
  let data = axios
    .post(`employer/delete-notifications`, { ids: notifications })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const readNotifications = (notifications) => {
  // type can be all, read, unread
  // make it handle
  let data = axios
    .post(`employer/read-notifications`, { ids: notifications })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const clearNotifications = (notifications) => {
  // type can be all, read, unread
  // make it handle
  let data = axios
    .post(`employer/delete-notifications`, { ids: notifications })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const getNotifications = (type = "all", params) => {
  let query = queryString.stringify(params);

  let data = axios
    .get(`employer/notifications/${type}?${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [], status: "failure" };
    });
  return data;
};

export const updateLeaveStatus = (leave_id, status) => {
  let data = axios
    .get(`employer/employee-leaves/${leave_id}/${status}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [] };
    });
  return data;
};

export const getLeaves = (query = "") => {
  let data = axios
    .get(`employer/employee-leaves${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [] };
    });
  return data;
};

export const getApplicantProfile = (id) => {
  let data = axios
    .get(`employer/employee-job-profile/${id}`)
    .then((res) => {
      let jobs = res.data;
      return jobs;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const updateShiftTimings = (data) => {
  let result = axios
    .post(`employer/update-shift-timing`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Shift setting updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data?.errors ?? null,
          message: res.data?.message,
        };
    })
    .catch((err) => {
      return { status: "error", message: "Shift setting update failed!" };
    });
  return result;
};
// start wallet and transactions
export const updateWalletSetting = (params) => {
  let query = queryString.stringify(params);
  let data = axios
    .get(`employer/employer-wallet-settings/update?${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getWalletSettings = () => {
  let data = axios
    .get(`employer/employer-wallet-settings`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getEmployerWalletTransactions = (query = "") => {
  let data = axios
    .get(`employer/employer-wallet-transactions${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const updateCard = (token_id, aditionalInfo) => {
  let result = axios
    .post(`employer/update-card`, { token_id, ...aditionalInfo })
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Card details updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ? res.data.message : "Card update failed!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Card details update failed!" };
    });
  return result;
};
// end wallet and transactions

export const createDispute = (data) => {
  let result = axios
    .post("employer/attendanceDispute", data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          data: res.data.data,
          status: "success",
          message: "Dispute submitted successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Dispute submission failed!" };
    });
  return result;
};

export const getEmployeeTimesheet = (applied_job_id, query) => {
  let data = axios
    .get(`employer/employee-timesheet/${applied_job_id}${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      //
      return { data: [] };
    });
  return data;
};

export const getEmployeeById = (applied_job_id) => {
  let data = axios
    .get(`employer/employees-list/${applied_job_id}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { data: [] };
    });

  return data;
};

export const getEmployees = (query = "") => {
  let data = axios
    .get(`employer/employees-list?${query}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      //
      return { data: [] };
    });

  return data;
};

export const updateWallet = (data) => {
  let result = axios
    .post(`employer/wallet-update`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data,
          message: "Wallet updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Wallet update failed!" };
    });
  return result;
};

export const deposit = (data) => {
  let result = axios
    .post(`employer/employerWithdrawDeposit`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Wallet updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Wallet updation failed!" };
    });
  return result;
};

export const getWallet = () => {
  let data = axios
    .get(`employer/employer-wallet`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      //
      return { data: [], error: "Error occured" };
    });
  return data;
};

// app settings start
export const getEmployerSettings = () => {
  let data = axios
    .get(`employer/appSettings`, {
      params: {
        keys: ["service_fee"],
        type: "employer",
      },
    })
    .then((res) => {
      let employees = res.data;
      return employees;
    })
    .catch((err) => {
      return { data: [], error: "Error occured" };
    });
  return data;
};

// app settings start
export const getAppSettings = () => {
  let data = axios
    .get(`employer/appSettings`, {
      params: {
        keys: [
          "interview_start_time",
          "interview_end_time",
          "employer_min_balance",
          "employer_max_balance",
          "service_fee",
        ],
        type: "app",
      },
    })
    .then((res) => {
      let employees = res.data;
      return employees;
    })
    .catch((err) => {
      //
      return { data: [], error: "Error occured" };
    });
  return data;
};
// app settings end

// API to get the employer basic info
export const getEmployerInfo = (id) => {
  let user = axios
    .get(`employer/profile/${id}`)
    .then((res) => {
      let user = res.data.data;
      return user;
    })
    .catch((err) => {
      //
      return { user: null };
    });
  //
  return user;
};

// API to get all the employer companies
export const getEmployerCompanies = () => {
  let user = axios
    .get(`employer/companies`)
    .then((res) => {
      let user = res.data;
      return user;
    })
    .catch((err) => {
      //
      return { user: null };
    });
  //
  return user;
};

export const updateProfile = (data, id, role) => {
  if (data.dob === "" || data.dob === null || data.dob === undefined)
    delete data.dob;
  let api = role === "admin" ? "admin/employers" : "employer/profile";

  let result = axios
    .patch(`${api}/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Profile updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", errors: {}, message: "Profile update failed!" };
    });
  return result;
};

// API to create/ update company
export const createCompany = (data) => {
  if (
    data.id === "" ||
    data.id === null ||
    data.id === undefined ||
    data.id === -1
  )
    delete data.id;
  let result = axios
    .post(`employer/companies`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Company created successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Company update failed!" };
    });
  return result;
};

export const editCompany = (data) => {
  if (
    data.id === "" ||
    data.id === null ||
    data.id === undefined ||
    data.id === -1
  )
    delete data.id;
  let result = axios
    .patch(`employer/companies/${data.id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Profile updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Profile update failed!" };
    });
  return result;
};

// Job Post APIS
export const getJobPosts = (query, resolveQueryParams) => {
  // resolve query params
  if (resolveQueryParams !== undefined) query = resolveQueryParams(query);
  let data = axios
    .get(`employer/postJob${query}`)
    .then((res) => {
      let jobs = res.data;
      return jobs;
    })
    .catch((err) => {
      //
      return { data: [] };
    });

  return data;
};
// Get Job Post Detail
export const getJobPostDetail = (id) => {
  // resolve query params
  let data = axios
    .get(`employer/postJob/${id}`)
    .then((res) => {
      let jobs = res.data.data;
      return jobs;
    })
    .catch((err) => {
      //
      return { data: [] };
    });

  return data;
};
// API to create
export const createJobPost = (data) => {
  if (
    data.id === "" ||
    data.id === null ||
    data.id === undefined ||
    data.id === -1
  )
    delete data.id;
  let result = axios
    .post(`employer/postJob`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Job posted successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Job post failed!" };
    });
  return result;
};
// api to edit
export const editJobPost = (data, id) => {
  if (
    data.id === "" ||
    data.id === null ||
    data.id === undefined ||
    data.id === -1
  )
    delete data.id;
  let result = axios
    .patch(`employer/postJob/${id}`, data)
    .then((res) => {
      if (res.data.status === "success") {
        return {
          status: "success",
          data: res.data,
          message: "Job updated successfully!",
        };
      } else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Job update failed!" };
    });
  return result;
};

export const deleteJobPost = (id) => {
  let result = axios
    .delete(`employer/jobPost/${id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Job post deleted successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,

          message: "Issue occured while removing!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Issue occured while deleting",
      };
    });
  return result;
};

export const getJobCandidates = (jobId, candidateStatuses, query = "") => {
  // let querystring =  queryString.stringify(candidateStatuses, {arrayFormat: 'bracket'})
  let queryParams = queryString.parse(query);
  queryParams.status = candidateStatuses.status;
  // let querystring =  queryString.stringify(candidateStatuses, {arrayFormat: 'bracket'})
  let querystring = queryString.stringify(queryParams, {
    arrayFormat: "bracket",
  });
  let data = axios
    .get(`employer/job-candidates/${jobId}?${querystring}`)
    .then((res) => {
      let applicants = res.data.data;

      return applicants;
    })
    .catch((err) => {
      //
      return { data: [] };
    });

  return data;
};

export const getJobSuggestedCandidates = (jobId, query = "") => {
  let data = axios
    .get(`employer/suggested-candidate/${jobId}${query}`)
    .then((res) => {
      let applicants = res.data.data;
      return applicants;
    })
    .catch((err) => {
      //
      return { data: [] };
    });

  return data;
};

// API to create
export const updateCandidateStatus = (job_id, data) => {
  let result = axios
    .post(`employer/update-candidate-status/${job_id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Data updated successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Candidate update failed!" };
    });
  return result;
};

export const checkCandidateHiredStatus = (applied_id) => {
  let result = axios
    .get(`employer/check-candidate-status/${applied_id}`)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "not hired",
        };
      else
        return {
          status: "hired"
        };
    })
    .catch((err) => {
      return { status: "error", message: "Status check failed!" };
    });
  return result;
};

export const shortlistCandidateFromSuggested = (data) => {
  let result = axios
    .post(`employer/shortlist-candidate`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data,
          message: "Candidate shortlisted successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Candidate shortlisting failed!" };
    });
  return result;
};

// send job_id
export const offerRequest = (job_id, data) => {
  let result = axios
    .post(`employer/send-offer/${job_id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Offer sent successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Offer request failed!" };
    });
  return result;
};

export const scheduleInterview = (data) => {
  let result = axios
    .post(`employer/schedule-interview`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Interview scheduled successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Interview scheduling failed!" };
    });
  return result;
};

export const updateInterview = (data) => {
  let result = axios
    .post(`employer/reschedule-interview`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Interview resechdule requested successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return {
        status: "error",
        errors: {},
        message: "Interview resechdule request failed!",
      };
    });
  return result;
};

export const sendOffer = (id, data) => {
  let result = axios
    .post(`employer/send-offer/${id}`, data)
    .then((res) => {
      if (res.data.status === "success")
        return {
          status: "success",
          data: res.data.data,
          message: "Offer sent successfully!",
        };
      else
        return {
          status: "error",
          errors: res.data.errors ? getErrors(res.data.errors) : null,
          message: res.data.message ?? "Kindly fix the data!",
        };
    })
    .catch((err) => {
      return { status: "error", message: "Offer sending failed!" };
    });
  return result;
};

export const getDashboardData = (__) => {
  return axios.get("employer/dashboard").then((result) => {
    return result.data;
  });
};

export const getLatestJobs = (__) => {
  return axios.get("employer/latestJobs").then((result) => {
    return result.data;
  });
};

export const getRecentInterviews = (__) => {
  return axios.get("employer/recentInterview").then((result) => {
    return result.data;
  });
};

export const employeesRecentAttendance = (__) => {
  return axios.get("employer/employeesAttendance").then((result) => {
    return result.data;
  });
};

export const employeesRecentLeaves = (__) => {
  return axios.get("employer/employeesLeaves").then((result) => {
    return result.data;
  });
};

export const getGoals = (__) => {
  return axios.get("employer/employeesLeaves").then((result) => {
    let temp = {};
    temp.data = [];
    return temp;
  });
};
export const getDashboardTeams = (__) => {
  return axios.get("employer/getTeams").then((result) => {
    return result.data;
  });
};

export const getAnnouncements = (__) => {
  return axios.get("employer/employeesLeaves").then((result) => {
    let temp = {};
    temp.data = [];
    return temp;
  });
};

export const resendVerification = () => {
  return axios.get("employer/resend-verification-email").then((result) => {
    return result.data;
  });
};
