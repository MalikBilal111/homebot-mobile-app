import React from "react";
// import { Typography, Chip, Button } from "@material-ui/core";
// import ArrowRightAlt from "@material-ui/icons/ArrowRightAlt";
// import Status from "../components/Status";

export const PERMISSIONS = {
  "Super Admin": [
    "url_candidates",
    "url_employees",
    "url_employers",
    "url_jobs",
    "url_interviews",
    "url_offers",
    "url_finance",
    "url_populates",
    "url_issues",
    "url_populates_localization",
    "url_populates_applicants",
    "url_populates_degrees",
    "url_populates_sources",
    "url_populates_institutes",
    "url_populates_industries",
    "url_populates_languages",
    "url_populates_skills",
    "url_populates_leaves",
    "url_system_settings",
    // add any more if it comes to
  ],
  "HR Manager": ["url_candidates", "url_issues"],
  "Account Manager": [
    "url_candidates",
    "url_employees",
    "url_employers",
    "url_jobs",
    "url_offers",
    "url_issues",
    "url_interviews",
  ],
};

export const ISSUE_CATEGORIES = [
  { id: 0, name: "Bug" },
  { id: 1, name: "Suggestion" },
  { id: 2, name: "Feedback" },
];

export const GENDERS_FOR_FILTERS = [
  //   { name: "No Preference", value: 0 },
  { name: "All", id: -1 },
  { name: "Male", id: 0 },
  { name: "Female", id: 1 },
];

export const EXPERIENCE_FOR_FILTERS = [
  { label: "1 Year", value: 1 },
  { label: "2 Years", value: 2 },
  { label: "3 Years", value: 3 },
  { label: "4 Years", value: 4 },
  { label: "+5 Years", value: 5 },
];

export const GENDERS = [
  { id: 0, name: "Male", value: 0 },
  { id: 1, name: "Female", value: 1 },
  { id: 2, name: "Not specified", value: 2 },
];

export const GENDERS_FOR_USERS = [
  { name: "Male", value: 0 },
  { name: "Female", value: 1 },
  { name: "Not specified", value: 2 },
];

export const USER_STATUS = [
  { id: 0, name: "Not Available", value: 0 },
  { id: 1, name: "Available", value: 1 },
];

// export const getUserStatus = (value) => {
//   switch (value) {
//     case 1:
//       return <Chip label="Available" className="pill-green" />;
//     case 0:
//       return <Chip label="Not Available" className="pill-pending" />;
//     default:
//       return <Chip label="NA" className="pill-pending" />;
//   }
// };

// export const getGenderByValue = (value) => {
//   let gender = GENDERS.filter((item) => item.value == parseInt(value))[0];
//   return gender === undefined ? null : gender;
// };

// export const getUserStatusByValue = (value) => {
//   let status = USER_STATUS.filter((item) => item.value == parseInt(value))[0];
//   return status === undefined ? null : status;
// };

// export const getGenderByName = (name) => {
//   let gender = GENDERS.filter((item) => item.name == name)[0];
//   return gender === undefined ? null : gender;
// };

export const DAYS = {
  0: "Sunday",
  1: "Monday",
  2: "Tueday",
  3: "Wednesday",
  4: "Thursday",
  5: "Friday",
  6: "Saturday",
};

// export const JOB_STATUS = [
//   { id: 0, name: "Pending", value: 0 },
//   { id: 1, name: "Approved", value: 1 },
//   { id: 2, name: "Rejected", value: 2 },
// ];

// export const getJobStatusById = (value) => {
//   return JOB_STATUS.filter((item) => item.value == parseInt(value))[0];
// };

// export const APPLICANT_STATUSES = [
//   { name: "applied", value: 0 },
//   { name: "shortlisted", value: 1 },
//   { name: "interview scheduled", value: 2 },
//   { name: "interviewed", value: 3 },
//   { name: "offer sent", value: 4 },
//   { name: "offer accepted", value: 5 },
//   { name: "offer rejected", value: 6 },
//   { name: "counter offer", value: 7 },
//   { name: "hired", value: 8 },
// ];

// export const getApplicantStatusByValue = (value) => {
//   return APPLICANT_STATUSES.filter((item) => item.value == parseInt(value))[0];
// };

// export const PROFILE_APPROVAL_STATUSES = [
//   { name: "Pending", value: 0 },
//   { name: "Approved", value: 1 },
//   { name: "Rejected", value: 2 },
// ];

// export const getProfileApprovalStatusByValue = (value) => {
//   return PROFILE_APPROVAL_STATUSES.filter(
//     (item) => item.value == parseInt(value)
//   )[0];
// };

// export const JOBPOST_APPROVAL_STATUSES = [
//   { id: 0, name: "Under Review", value: 0 },
//   { id: 1, name: "Approved", value: 1 },
//   { id: 2, name: "Rejected", value: 2 },
//   { id: 3, name: "Expired", value: 3 },
// ];

// export const getJobPostApprovalStatusByValue = (value) => {
//   return JOBPOST_APPROVAL_STATUSES.filter(
//     (item) => item.value == parseInt(value)
//   )[0];
// };

// export const ADMIN_INTERVIEW_STATUSES = [
//   { id: 0, name: "Pending", value: 0 },
//   { id: 1, name: "Scheduled", value: 1 },
//   { id: 2, name: "Employee Reschedule Request", value: 2 },
//   { id: 3, name: "Completed", value: 3 },
//   { id: 4, name: "Employee No Show", value: 4 },
//   { id: 5, name: "Employer No Show", value: 5 },
//   { id: 6, name: "Cancelled", value: 6 },
//   { id: 7, name: "Rejected", value: 7 },
//   { id: 8, name: "Expired", value: 8 },
// ];

// export const INTERVIEW_STATUSES = [
//   { name: "Pending", value: 0 },
//   { name: "Scheduled", value: 1 },
//   { name: "Reschedule Request", value: 2 },
//   { name: "Completed", value: 3 },
//   { name: "Employee No Show", value: 4 },
//   { name: "Employer No Show", value: 5 },
//   { name: "Cancelled", value: 6 },
//   { name: "Rejected", value: 7 },
//   { name: "Expired", value: 8 },
// ];

// export const getInterviewStatusByValue = (value) => {
//   return INTERVIEW_STATUSES.filter((item) => item.value == parseInt(value))[0];
// };

// // export const getEmployeeInteriewStatusChip = (value, role) => {
// //   let name = "";
// //   if (role === "admin")
// //     name = ADMIN_INTERVIEW_STATUSES.filter(
// //       (item) => item.value == parseInt(value)
// //     )[0]?.name;
// //   else
// //     name = INTERVIEW_STATUSES.filter((item) => item.value == parseInt(value))[0]
// //       ?.name;

// //   switch (name) {
// //     case "Scheduled":
// //       return (
// //         <Chip
// //           label="Scheduled"
// //           className={"pill-success"}
// //           // style={{ backgroundColor: '#4AACEB', color: '#fff' }}
// //         />
// //       );
// //     case "Reschedule Request":
// //       return (
// //         <Chip
// //           label="Reschedule Requested"
// //           className={"pill-pending"}
// //           // style={{ backgroundColor: '#ffc107', color: '#fff' }}
// //         />
// //       );
// //     case "Employee Reschedule Request":
// //       return (
// //         <Chip
// //           label="Reschedule Requested"
// //           className={"pill-pending"}
// //           // style={{ backgroundColor: '#ffc107', color: '#fff' }}
// //         />
// //       );
// //     case "Completed":
// //       return (
// //         <Chip
// //           label="Completed"
// //           className={"pill-success"}
// //           // style={{ backgroundColor: '#00c853', color: '#fff' }}
// //         />
// //       );
// //     case "Employee No Show":
// //       return (
// //         <Chip
// //           label="Employee No Show"
// //           className={"pill-danger"}
// //           // style={{ backgroundColor: '#ffc107', color: '#fff' }}
// //         />
// //       );
// //     case "Employer No Show":
// //       return (
// //         <Chip
// //           label="Employer No Show"
// //           className={"pill-pending"}
// //           // style={{ backgroundColor: '#ffc107', color: '#fff' }}
// //         />
// //       );
// //     case "Rejected":
// //       return (
// //         <Chip
// //           label="Rejected"
// //           className={"pill-danger"}
// //           // style={{ backgroundColor: '#FC443E', color: '#fff' }}
// //         />
// //       );
// //     case "Cancelled":
// //       return (
// //         <Chip
// //           label="Cancelled"
// //           className={"pill-danger"}
// //           // style={{ backgroundColor: '#FC443E', color: '#fff' }}
// //         />
// //       );
// //     case "Expired":
// //       return (
// //         <Chip
// //           label="Expired"
// //           className={"pill-danger"}
// //           // style={{ backgroundColor: '#FC443E', color: '#fff' }}
// //         />
// //       );
// //     default:
// //       return (
// //         <Chip
// //           label="Pending"
// //           className={"pill-pending"}
// //           // style={{ backgroundColor: '#ffc107', color: '#fff' }}
// //         />
// //       );
// //   }
// // };

// export const getEmployeeInteriewStatus = (value) => {
//   let name = INTERVIEW_STATUSES.filter(
//     (item) => item.value == parseInt(value)
//   )[0]?.name;
//   switch (name) {
//     case "Scheduled":
//       return <Status text="Scheduled" status="success" />;
//     case "Reschedule Request":
//       return <Status text="Reschedule Requested" status="info" />;
//     case "Completed":
//       return <Status text="Completed" status="success" />;
//     case "Expired":
//       return <Status text="Expired" status="danger" />;
//     case "Rejected":
//       return <Status text="Rejected" status="danger" />;
//     case "Cancelled":
//       return <Status text="Cancelled" status="danger" />;
//     default:
//       return <Status text="Pending" status="pending" />;
//   }
// };

// export const LEAVE_STUSES = [
//   { value: 0, name: "Pending" },
//   { value: 1, name: "Approved" },
//   { value: 2, name: "Rejected" },
// ];

// export const getLeaveStatuses = (value) => {
//   let result = LEAVE_STUSES.filter((item) => item.value == parseInt(value))[0];
//   return result === undefined ? null : result;
// };

// // export const popupObject = (
// //   title,
// //   text,
// //   btnText,
// //   imageIcon,
// //   titleColor = "#006AB3"
// // ) => {
// //   return {
// //     title: (
// //       <Typography
// //         style={{
// //           fontFamily: "Lato",
// //           color: titleColor,
// //           fontSize: "1.75rem",
// //           fontWeight: "bold",
// //           paddingTop: "5rem 0",
// //         }}
// //       >
// //         {title}
// //       </Typography>
// //     ),
// //     html: (
// //       <Typography
// //         style={{
// //           fontFamily: "Lato",
// //           color: "#000000",
// //           fontSize: "1rem",
// //           paddingBottom: "3rem",
// //           margin: "0px auto",
// //           width: "53rem",
// //           textAlign: "center",
// //         }}
// //       >
// //         {text}
// //       </Typography>
// //     ),
// //     width: "70rem",
// //     closeButtonHtml: <span>&times;</span>,
// //     imageUrl: imageIcon,
// //     heightAuto: false,
// //     showCloseButton: true,
// //     confirmButtonText: (
// //       <Button endIcon={<ArrowRightAlt />} style={{ color: "white" }}>
// //         {btnText}
// //       </Button>
// //     ),
// //   };
// // };

// export const JOB_STATUSES = [
//   { id: 8, name: "Hired", value: 8 },
//   { id: 9, name: "Active", value: 9 },
//   { id: 11, name: "Terminated", value: 11 },
//   { id: 12, name: "Resigned", value: 12 },
//   { id: 13, name: "Disabled Checkin", value: 13 },
//   { id: 14, name: "Suspended", value: 14 },
// ];

// // export const getEmployeeJobStatus = (value) => {
// //   switch (value) {
// //     case 8:
// //       return <Chip label="Hired" className="pill-rl-status-approved" />;
// //     case 9:
// //       return <Chip label="Active" className="pill-rl-status-approved" />;
// //     case 11:
// //       return <Chip label="Terminated" className="pill-rl-status-rejected" />;
// //     case 12:
// //       return <Chip label="Resigned" className="pill-rl-status-rejected" />;
// //     case 13:
// //       return (
// //         <Chip label="Disabled Checkin" className="pill-rl-status-rejected" />
// //       );
// //     case 14:
// //       return <Chip label="Suspended" className="pill-rl-status-rejected" />;
// //     default:
// //       return <Chip label="NA" className="pill-rl-status-pending" />;
// //   }
// // };

// export const JOB_OFFER_STATUSES = [
//   { id: 4, name: "Pending", value: 4 },
//   { id: 5, name: "Accepted", value: 5 },
//   { id: 6, name: "Rejected", value: 6 },
//   { id: 7, name: "Countered", value: 7 },
//   { id: 8, name: "Hired", value: 8 },
//   { id: 9, name: "Joined", value: 9 },
//   { id: 10, name: "Closed", value: 10 },
//   { id: 11, name: "Terminated", value: 11 },
//   { id: 12, name: "Resigned", value: 12 },
//   { id: 13, name: "Checkin Disabled", value: 13 },
//   { id: 14, name: "Suspended", value: 14 },
//   { id: 15, name: "Expired", value: 15 },
//   { id: 16, name: "Expired", value: 16 },
// ];

// export const getJobOfferStatus = (value) => {
//   switch (value) {
//     case 4:
//       return <Chip label="Pending" className="pill-pending" />;
//     case 5:
//       return <Chip label="Accepted" className="pill-success" />;
//     case 6:
//       return <Chip label="Rejected" className="pill-danger" />;
//     case 7:
//       return <Chip label="Countered" className="pill-info" />;
//     case 8:
//       return <Chip label="Hired" className="pill-success" />;
//     case 9:
//       return <Chip label="Joined" className="pill-success" />;
//     case 10:
//       return <Chip label="Closed" className="pill-danger" />;
//     case 11:
//       return <Chip label="Terminated" className="pill-danger" />;
//     case 12:
//       return <Chip label="Resigned" className="pill-danger" />;
//     case 13:
//       return <Chip label="Checkin Disabled" className="pill-pending" />;
//     case 14:
//       return <Chip label="Suspended" className="pill-danger" />;
//     case 15:
//       return <Chip label="Expired" className="pill-danger" />;
//     case 16:
//       return <Chip label="Expired" className="pill-danger" />;
//     default:
//       return <Chip label="NA" />;
//   }
// };
