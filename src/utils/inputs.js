import { runValidator } from "./validations";
import clonedeep from "lodash.clonedeep";

export const handleValidation = (e, errorsList, setErrorFunc, inputsList) => {
  // let errors = runValidator(e.target.value, rules)
  let newErrors = runValidator(e.target.value, inputsList[e.target.name].rules);
  let errorMessage = inputsList[e.target.name].errorMessage;
  if (newErrors.length > 0) {
    setErrorFunc({ ...errorsList, [e.target.name]: errorMessage });
    //   this.setState({
    //     errors: { ...uppdatedErrors, [e.target.name]: errorMessage },
    //   });
  } else {
    let uppdatedErrors = clonedeep(errorsList);
    delete uppdatedErrors[e.target.name];
    setErrorFunc({ ...uppdatedErrors });
    // this.setState({ errors: uppdatedErrors });
  }
};

export const validateAll = (inputsObject, validationsList) => {
  let errors = {};
  for (let field in validationsList) {
    let fieldErrors = runValidator(
      inputsObject[field],
      validationsList[field].rules
    );
    let errorMessage = validationsList[field].errorMessage;
    if (fieldErrors.length > 0) {
      errors[field] = errorMessage;
    }
  }
  return errors;
};
