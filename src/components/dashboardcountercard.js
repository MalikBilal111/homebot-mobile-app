import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Svg, { G, Circle, Path } from "react-native-svg";
class dashboardcountercard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const styles = StyleSheet.create({
      cardMain: {
        backgroundColor: "white",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10,
        width: 103,
        padding:10
      },
      cardTitle: {
        fontSize: 12,
        marginTop: 5,
        color: "#495057",
      },
      cardNumber: {
        fontSize: 32,
        // fontWeight: "bold",
        marginTop: 13,
        marginBottom: 10,
        fontWeight:"bold"
      },
    });
    const { title, count, index, navigateto } = this.props;
    return (
      <>
        <View
          key={title + "Counter-" + count}
          style={[styles.cardMain, index != 0 ? { marginLeft: 5 } : null]}
        >
          <DashboardEmployeesIcon />
          <Text style={styles.cardTitle}>{title}</Text>
          <Text style={styles.cardNumber} onPress={navigateto}>
            {count != undefined
              ? count.toString().length == 1
                ? "0" + count
                : count
              : null}
          </Text>
        </View>
      </>
    );
  }
}

export default dashboardcountercard;

function DashboardEmployeesIcon() {
  return (
    <Svg xmlns="http://www.w3.org/2000/svg" width={30} height={30}>
      <G data-name="Group 3048" transform="translate(-169 -230)">
        <Circle
          data-name="Ellipse 495"
          cx={15}
          cy={15}
          r={15}
          transform="translate(169 230)"
          fill="#edfaef"
        />
        <Path
          data-name="Icon awesome-users"
          d="M179.767 244.596a1.2 1.2 0 000-2.4 1.2 1.2 0 000 2.4zm8.248 0a1.2 1.2 0 10-1.178-1.2 1.19 1.19 0 001.178 1.2zm.589.6h-1.178a1.165 1.165 0 00-.83.348 2.74 2.74 0 011.382 2.049h1.215a.594.594 0 00.589-.6v-.6a1.19 1.19 0 00-1.182-1.197zm-4.713 0a2.1 2.1 0 10-2.062-2.1 2.079 2.079 0 002.062 2.1zm1.414.6h-.153a2.8 2.8 0 01-2.522 0h-.153a2.14 2.14 0 00-2.121 2.154v.539a.892.892 0 00.884.9h5.3a.892.892 0 00.884-.9v-.539a2.14 2.14 0 00-2.119-2.155zm-4.118-.252a1.165 1.165 0 00-.83-.348h-1.179a1.19 1.19 0 00-1.178 1.198v.6a.594.594 0 00.589.6h1.211a2.747 2.747 0 011.387-2.05z"
          fill="#47cb5d"
        />
      </G>
    </Svg>
  );
}

function JobOffersIcon() {
  return (
    <Svg
      style={{ marginTop: 8 }}
      xmlns="http://www.w3.org/2000/svg"
      width={30}
      height={30}
    >
      <G data-name="Group 3048" transform="translate(-169 -230)">
        <Circle
          data-name="Ellipse 495"
          cx={15}
          cy={15}
          r={15}
          transform="translate(169 230)"
          fill="#edfaef"
        />
        <Path
          d="M190.117 239L178 245.816l3.872 1.434 5.973-5.6-4.545 6.129v3.337l2.173-2.533 2.75 1.017z"
          fill="#47cb5d"
        />
      </G>
    </Svg>
  );
}
