import React, { Component } from "react";
import {
  Title,
  Button,
  TextInput,
  Paragraph,
  TouchableRipple
} from "react-native-paper";
import { StyleSheet, View, Text, ScrollView, SafeAreaView } from "react-native";
import theme from "../../styles/theme";
import RegisterScreenIcon from "../../assets/svg/registerscreenicon";
import FingerPrint from "../../assets/svg/fingerprint";
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secureTextEntry: false,
    };
  }

  handleInputChange = (name, value) => {
    this.setState({ [name]: value });
  };

  navigateToRegister=(screenname)=>{
    this.props.navigation.navigate(screenname);
  }

  render() {
    const styles = StyleSheet.create({
      finger_print_button:
      {
        width: "30%",
        backgroundColor: "#DFEBF9",
        borderRadius: 8,
        justifyContent: "center",
        alignItems: "center"
      }
    });
    const { secureTextEntry } = this.state;
    return (
      <>
        <ScrollView style={{ marginTop: 50 }}>
          <View style={theme.container_margin}>
            <RegisterScreenIcon />
            <View style={theme.margin5}>
              <Paragraph style={theme.secondary_title}>JOIN THE TEAM</Paragraph>
              <Title style={theme.mainTitle}>Create Account</Title>
            </View>
            {/* <View style={theme.margin5}>
              <Paragraph
                style={[theme.secondary_title, { textAlign: "center" }]}
              >
                Or Log In Using Email
              </Paragraph>
            </View> */}
            <TextInput
              label="First Name"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Last Name"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Username"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Email"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Password"
              style={theme.textField}
              mode="outlined"
              activeOutlineColor={theme.palette.primary.main}
              right={
                <TextInput.Icon
                  name={secureTextEntry ? "eye" : "eye-off"}
                  onPress={() =>
                    this.handleInputChange(
                      "secureTextEntry",
                      !this.state.secureTextEntry
                    )
                  }
                  color="#707070"
                  style={{ marginTop: 15 }}
                />
              }
              outlineColor="#707070"
              secureTextEntry={secureTextEntry}
            />
            <TextInput
              label="Confirm Password"
              style={theme.textField}
              mode="outlined"
              activeOutlineColor={theme.palette.primary.main}
              right={
                <TextInput.Icon
                  name={secureTextEntry ? "eye" : "eye-off"}
                  onPress={() =>
                    this.handleInputChange(
                      "secureTextEntry",
                      !this.state.secureTextEntry
                    )
                  }
                  color="#707070"
                  style={{ marginTop: 15 }}
                />
              }
              outlineColor="#707070"
              secureTextEntry={secureTextEntry}
            />
            <View style={theme.margin5}>
            <Button
              mode="contained"
              uppercase={false}
              contentStyle={[theme.button_container, { backgroundColor: theme.palette.primary.main }]}
              style={theme.button_border_radius}
              onPress={()=>this.navigateToRegister("VerficationScreen")}
            >
              Register
            </Button>
            </View>
            <TouchableRipple rippleColor="transparent" onPress={()=>this.navigateToRegister("LoginUser")} >
            <View style={theme.margin5}>
              <Paragraph
                style={[theme.secondary_title, { textAlign: "center" }]}
              >
                Already a Team Member? <Text style={{ fontWeight: "bold", color: theme.palette.primary.main }}>LOG IN</Text>
              </Paragraph>
            </View>
            </TouchableRipple>
            <View style={theme.margin5}></View>
          </View>
        </ScrollView>
      </>
    );
  }
}

export default index;
