import React, { Component } from "react";
import {
  Title,
  Button,
  TextInput,
  Paragraph,
  TouchableRipple
} from "react-native-paper";
import { StyleSheet, View, Text, ScrollView, SafeAreaView } from "react-native";
import theme from "../../styles/theme";
import LoginScreenIcon from "../../assets/svg/loginscreenicon";
import FingerPrint from "../../assets/svg/fingerprint";
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secureTextEntry: false,
    };
  }

  handleInputChange = (name, value) => {
    this.setState({ [name]: value });
  };

  navigateToRegister=()=>{
    this.props.navigation.navigate("RegisterScreen");
  }

  render() {
    const styles = StyleSheet.create({
      finger_print_button:
      {
        width: "30%",
        backgroundColor: "#DFEBF9",
        borderRadius: 8,
        justifyContent: "center",
        alignItems: "center"
      }
    });
    const { secureTextEntry } = this.state;
    return (
      <>
        <SafeAreaView style={theme.container}>
          <ScrollView style={[theme.container_margin, { marginTop: 50 }]}>
            <LoginScreenIcon />
            <View style={theme.margin5}>
              <Paragraph style={theme.secondary_title}>WELCOME BACK</Paragraph>
              <Title style={theme.mainTitle}>Account Log In</Title>
            </View>
            <TextInput
              label="Email"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Password"
              style={theme.textField}
              mode="outlined"
              activeOutlineColor={theme.palette.primary.main}
              right={
                <TextInput.Icon
                  name={secureTextEntry ? "eye" : "eye-off"}
                  onPress={() =>
                    this.handleInputChange(
                      "secureTextEntry",
                      !this.state.secureTextEntry
                    )
                  }
                  color="#707070"
                  style={{ marginTop: 15 }}
                />
              }
              outlineColor="#707070"
              secureTextEntry={secureTextEntry}
            />
            <Text
              onPress={this.toggleForgotPassword}
              style={{ textAlign: "right", marginTop: 10, marginBottom: 10 }}
            >
              Forgot Password?
            </Text>
            <View style={{ flexDirection: "row",marginBottom:15 }}>
              <View style={{ width: "65%" }}>
                <Button
                  mode="contained"
                  uppercase={false}
                  contentStyle={[theme.button_container, { backgroundColor: theme.palette.primary.main }]}
                  style={theme.button_border_radius}
                >
                  Login
                </Button>
              </View>
              <View style={{ width: "5%" }}></View>
              <View style={styles.finger_print_button}>
                  <FingerPrint />
              </View>
            </View>
            <View style={theme.margin5}>
              <TouchableRipple rippleColor="transparent" onPress={this.navigateToRegister} >
              <Paragraph
                style={[theme.secondary_title, { textAlign: "center" }]}
              >
                Don’t have an Account? <Text style={{fontWeight:"bold", color:theme.palette.primary.main}}>REGISTER</Text>
              </Paragraph>
              </TouchableRipple>
            </View>
            {/* <View style={theme.margin5}>
              <Paragraph
                style={[theme.secondary_title, { textAlign: "center" }]}
              >
                Or Log In Using Social Accounts
              </Paragraph>
            </View> */}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

export default index;
