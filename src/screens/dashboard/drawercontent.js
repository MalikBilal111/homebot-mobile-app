import React, { Component } from "react";
import { List, Divider } from "react-native-paper";
import theme from "../../styles/theme";
import { StyleSheet, View, Text, ScrollView, SafeAreaView } from "react-native";
import Svg, { Path, G } from "react-native-svg";
import DrawerLayout from "../../assets/svg/layoutdrawer";
class drawercontent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <SafeAreaView style={theme.container}>
          <ScrollView key="DrawerContent-ViewParent">
            {/* <DrawerLayout {...this.props} /> */}
            {/* <View
              key="DrawerContent-Child"
              style={{ marginLeft: 20, marginRight: 20 }}
            >
              {menuItems &&
                menuItems.map((item, index) =>
                  item.title == "Divider" ? (
                    <Divider key={"Divider" + item.title + "-" + index} />
                  ) : (
                    <List.Item
                      key={item.title + "-" + index}
                      title={item.title}
                      onPress={()=>this.navigateToRoute(item.navigation)}
                      left={(props) => item.leftIcon}
                      right={(props) =>
                        item.rightIcon != "Balance" ? (
                          item.rightIcon
                        ) : (
                          <Text style={{ marginTop: 12, color: "#006AB3" }}>
                            {"$" + this.props.employerInfo?.balance}
                          </Text>
                        )
                      }
                    />
                  )
                )}
            </View> */}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

// export default drawercontent;

// const menuItems = [
//   {
//     title: "Current Balance",
//     leftIcon: <DollarIcon />,
//     rightIcon: "Balance",
//   },
//   {
//     title: "Add Funds",
//     navigation:"AddFunds",
//     leftIcon: <FundsIcon />,
//     rightIcon: null,
//   },
//   {
//     title: "Divider",
//     leftIcon: "divider",
//     rightIcon: null,
//   },
//   {
//     title: "Teams",
//     leftIcon: <TeamsIcon />,
//     navigation:"AddFunds",
//     rightIcon: <ArrowIcon />,
//   },
//   {
//     title: "Leaves",
//     leftIcon: <LeavesIcon />,
//     navigation:"Leaves",
//     rightIcon: <ArrowIcon />,
//   },
//   {
//     title: "Reports",
//     leftIcon: <ReportsIcon />,
//     navigation:"Leaves",
//     rightIcon: <ArrowIcon />,
//   },
//   {
//     title: "Divider",
//     leftIcon: "divider",
//     rightIcon: null,
//   },
//   {
//     title: "Support",
//     leftIcon: <SupportIcon />,
//     navigation:"Leaves",
//     rightIcon: <ShareIcon />,
//   },
//   {
//     title: "Settings",
//     leftIcon: <SettingIcon />,
//     navigation:"Leaves",
//     rightIcon: <ShareIcon />,
//   },
//   {
//     title: "Divider",
//     leftIcon: "divider",
//     rightIcon: null,
//   },
//   {
//     title: "Sign out",
//     leftIcon: <SignoutIcon />,
//     navigation:"Leaves",
//     rightIcon: null,
//   },
// ];

function ArrowIcon() {
  return (
    <Svg
      style={{ marginTop: 12 }}
      xmlns="http://www.w3.org/2000/svg"
      width={9.214}
      height={16.428}
    >
      <Path
        data-name="Icon feather-chevron-right"
        d="M1.414 15.014l6.8-6.8-6.8-6.8"
        fill="none"
        stroke="#000"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
    </Svg>
  );
}

function ShareIcon() {
  return (
    <Svg
      style={{ marginTop: 12 }}
      xmlns="http://www.w3.org/2000/svg"
      width={17.352}
      height={17.352}
    >
      <G data-name="Group 3254">
        <G data-name="Group 3253">
          <Path
            data-name="Path 10491"
            fill="none"
            stroke="#000"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1}
            d="M12.074 1.214h3.2L7.95 8.541a.607.607 0 10.857.86l7.327-7.327v3.2a.607.607 0 101.214 0V.609A.609.609 0 0016.743 0h-4.669a.607.607 0 100 1.214z"
          />
          <Path
            data-name="Path 10492"
            fill="none"
            stroke="#000"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1}
            d="M3.642 17.352h10.064a3.646 3.646 0 003.642-3.642V8.97a.607.607 0 00-1.214 0v4.736a2.432 2.432 0 01-2.428 2.428H3.642a2.432 2.432 0 01-2.428-2.428V3.646a2.432 2.432 0 012.428-2.428h4.7A.609.609 0 008.34 0h-4.7A3.646 3.646 0 000 3.642v10.064a3.649 3.649 0 003.642 3.646z"
          />
        </G>
      </G>
    </Svg>
  );
}


export default drawercontent;
