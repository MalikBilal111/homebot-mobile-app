import React, { Component } from "react";
// Drawer Navigator
import { createDrawerNavigator } from "@react-navigation/drawer";
import Dashboard from "./bottomnavigation";
import DrawerContent from "./drawercontent";
//Leave Screen
// import LeaveScreenEmployee from "../employer/leaves/index";
// import EmployeeTimeSheet from "../employer/timesheet/index";
// import AddFunds from "../employer/addfunds/index";
const Drawer = createDrawerNavigator();
class drawer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <Drawer.Navigator
          initialRouteName="Dashboard"
          drawerContent={props=><DrawerContent {...props}/>}
          screenOptions={{
            headerShown: false,
            drawerStyle: {
              width: "100%",
            },
          }}
        >
          <Drawer.Screen name="Dashboard" component={Dashboard}/>
          {/* <Drawer.Screen name="Leaves" component={LeaveScreenEmployee} />
          <Drawer.Screen name="AddFunds" component={AddFunds} />
          <Drawer.Screen name="EmployerTimeSheet" component={EmployeeTimeSheet} /> */}
        </Drawer.Navigator>
      </>
    );
  }
}

export default drawer;
