import React, { Component } from "react";
//Theme
import theme from "../../styles/theme";
// BottomTabNavigation
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
//Icons for bottom navigation
import {
  JobsIcon,
  HomeIcon,
  WalletIcon,
  EmployeeIcon,
  JobIcon,
  EmployeeProfileIcon,
  EmployeeMyJobs,
  EmployeeSearchJobs,
} from "../../assets/svg/dashboardicon";
import { View, Text} from "react-native";
import Home from "./home/index";
const Tab = createBottomTabNavigator();
class bottomnavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Tab.Navigator
        initialRouteName="HomeScreen"
        screenOptions={{
          tabBarActiveTintColor: theme.palette.primary.main,
          headerShown: false,
          tabBarStyle: {
            marginBotton: 30,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
            paddingBottom: 20,
            paddingTop: 20,
            height: 80,
            // position:"absolute",
            zIndex: 99,
          },
        }}
      >
        <Tab.Screen
          name="HomeScreen"
          options={{
            tabBarLabel: "Home",
            tabBarInactiveTintColor: "#B0BED4",
            tabBarIcon: ({ color, size }) => <HomeIcon color={color} />,
          }}
          component={Home}
        />
      </Tab.Navigator>
    );
  }
}


export default bottomnavigation;
