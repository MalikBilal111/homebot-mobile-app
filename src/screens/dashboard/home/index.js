import React, { Component } from "react";
import {
  Title,
  Button,
  TextInput,
  Paragraph,
  TouchableRipple,
} from "react-native-paper";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Dimensions,
} from "react-native";
import theme from "../../../styles/theme";
import LayoutOne from "../../../assets/svg/layoutone";
import CounterCard from "../../../components/dashboardcountercard";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  navigateUpperCard = () => {};
  render() {
    const styles = StyleSheet.create({
      card_title: {
        textAlign: "center",
        color: "white",
        padding:4,
      },
    });
    return (
      <>
        <LayoutOne title="Home" />
        <SafeAreaView style={theme.container}>
          <ScrollView style={theme.container_margin}>
            <View style={theme.margin5}>
              <Title style={theme.mainTitle}>Good Morning Mate!</Title>
              <Paragraph style={theme.secondary_title}>
                Have a nice day
              </Paragraph>
            </View>
            <View style={[theme.margin5, { flexDirection: "row" }]}>
              <CounterCard title="Total Revenue" count={12} index={0} />
              <CounterCard title="Total Scenes" count={16} index={1} />
              <CounterCard title="Total Members" count={8} index={2} />
            </View>
            <View
              style={[
                theme.margin5,
                { backgroundColor: theme.palette.primary.main,borderRadius:8 },
              ]}
            >
              <Title style={styles.card_title}>Weather</Title>
              <View style={{ backgroundColor: "white",padding:10,borderRadius:8 }}>
                <LineChart
                  data={{
                    labels: [
                      "Jan",
                      "Feb",
                      "Mar",
                      "Apr",
                      "May",
                      "June",
                    ],
                    datasets: [
                      {
                        data: [
                          Math.random() * 100,
                          Math.random() * 100,
                          Math.random() * 100,
                          Math.random() * 100,
                          Math.random() * 100,
                          Math.random() * 100,
                        ],
                      },
                    ],
                  }}
                  width={screenWidth-65} // from react-native
                  height={220}
                  yAxisLabel="$"
                  yAxisSuffix="k"
                  yAxisInterval={1} // optional, defaults to 1
                  chartConfig={{
                    backgroundColor: "#e26a00",
                    backgroundGradientFrom: "#fb8c00",
                    backgroundGradientTo: "#ffa726",
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) =>
                      `rgba(255, 255, 255, ${opacity})`,
                    style: {
                      borderRadius: 16,
                    },
                    propsForDots: {
                      r: "6",
                      strokeWidth: "2",
                      stroke: "#ffa726",
                    },
                  }}
                  bezier
                  style={{
                    // marginVertical: 8,
                    borderRadius: 16,
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

export default index;

const data = {
  labels: ["January", "February", "March", "April", "May", "June"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
    },
  ],
};

const chartConfig = {
  // backgroundGradientFrom: "#1E2923",
  // backgroundGradientFromOpacity: 0,
  // backgroundGradientTo: "#08130D",
  // backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false, // optional
};

const screenWidth = Dimensions.get("window").width;
