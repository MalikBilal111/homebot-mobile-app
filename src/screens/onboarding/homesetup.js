import React, { Component } from "react";
import {
  Title,
  Button,
  TextInput,
  Paragraph,
  TouchableRipple,
  IconButton,
} from "react-native-paper";
import { StyleSheet, View, Text, ScrollView, SafeAreaView } from "react-native";
import theme from "../../styles/theme";
import HomeSetupIcon from "../../assets/svg/homesetup";

class homesetup extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onChangeHandler = (name, value) => {
    this.setState({ [name]: value });
  };

  navigateToFingerprint = () => {
    this.props.navigation.navigate("WelcomeScreen");
  };

  render() {
    const styles = StyleSheet.create({
      text_align_center: { textAlign: "center" },
    });
    return (
      <>
        <ScrollView style={theme.container}>
          <View style={theme.container_margin}>
            <View style={theme.align_center}>
              <HomeSetupIcon />
            </View>
            <View style={theme.margin5}>
              <Title style={[theme.mainTitle, styles.text_align_center]}>
                Home Setup
              </Title>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                Enter your personal and contact details to
              </Paragraph>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                account creation. You have
              </Paragraph>
            </View>
            <TextInput
              label="House Name"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Country"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="City"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Zip Code"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Package"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <TextInput
              label="Address"
              mode="outlined"
              style={theme.textField}
              autoCapitalize="none"
              outlineColor="#707070"
              activeOutlineColor={theme.palette.primary.main}
            />
            <View style={theme.margin5}>
              <Button
                mode="contained"
                uppercase={false}
                contentStyle={[
                  theme.button_container,
                  { backgroundColor: theme.palette.primary.main },
                ]}
                style={theme.button_border_radius}
                onPress={this.navigateToFingerprint}
              >
                Submit
              </Button>
            </View>
            <View style={theme.margin5}></View>
          </View>
        </ScrollView>
      </>
    );
  }
}

export default homesetup;
