import React, { Component } from "react";
import {
  Title,
  Button,
  TextInput,
  Paragraph,
  TouchableRipple,
  IconButton,
} from "react-native-paper";
import { StyleSheet, View, Text, ScrollView, SafeAreaView } from "react-native";
import theme from "../../styles/theme";
import FingerPrintSetupIcon from "../../assets/svg/fingerprintsetup";
import FingerPrintRed from "../../assets/svg/fingerprintred";

class fingerprintsetup extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onChangeHandler = (name, value) => {
    this.setState({ [name]: value });
  };

  navigateToHomeSetup = () => {
    this.props.navigation.navigate("HomeSetup");
  };

  render() {
    const styles = StyleSheet.create({
      text_align_center: { textAlign: "center" },
    });
    return (
      <>
        <ScrollView>
          <View style={[theme.container, theme.align_center]}>
            <View style={theme.align_center}>
              <FingerPrintSetupIcon />
            </View>
            <View style={theme.margin5}>
              <Title style={[theme.mainTitle, styles.text_align_center]}>
                Fingerprint
              </Title>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                Rest Your Finger At The Sensor
              </Paragraph>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                To Capture Your Fingerprint
              </Paragraph>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                (Optional)
              </Paragraph>
            </View>
            <View style={{ marginTop: 20, marginBottom: 20 }}>
              <FingerPrintRed />
            </View>
            <View style={theme.margin5}>
              <Button
                mode="contained"
                uppercase={false}
                contentStyle={[
                  theme.button_container,
                  { backgroundColor: theme.palette.primary.main, width: 200 },
                ]}
                style={theme.button_border_radius}
                onPress={this.navigateToHomeSetup}
              >
                Continue
              </Button>
            </View>
            <TouchableRipple
              rippleColor="transparent"
              onPress={this.navigateToHomeSetup}
            >
              <View style={theme.margin5}>
                <Paragraph
                  style={[
                    theme.secondary_title,
                    { textAlign: "center", color: theme.palette.primary.main },
                  ]}
                >
                  Skip This Step
                </Paragraph>
              </View>
            </TouchableRipple>
            <View style={theme.margin5}></View>
          </View>
        </ScrollView>
      </>
    );
  }
}

export default fingerprintsetup;
