import React, { Component } from "react";
import {
  Title,
  Button,
  TextInput,
  Paragraph,
  TouchableRipple,
  IconButton,
} from "react-native-paper";
import { StyleSheet, View, Text, ScrollView, SafeAreaView } from "react-native";
import theme from "../../styles/theme";
import VerificationIcon from "../../assets/svg/verificationicon";
class verification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focus_input_field: "0",
    };
  }

  onChangeHandler = (name, value) => {
    this.setState({ [name]: value });
  };

  navigateToFingerprint = () => {
    this.props.navigation.navigate("FingerPrintSetup");
  };

  render() {
    const styles = StyleSheet.create({
      text_align_center: { textAlign: "center" },
      code_box: {
        margin: 20,
        padding: 15,
        backgroundColor: "white",
        borderRadius: 8,
      },
    });
    const { focus_input_field } = this.state;
    return (
      <>
        <ScrollView>
          <View style={[theme.container, theme.align_center]}>
            <View style={theme.align_center}>
              <VerificationIcon />
            </View>
            <View style={theme.margin5}>
              <Title style={[theme.mainTitle, styles.text_align_center]}>
                Verification
              </Title>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                Enter A 4 Digit Number That
              </Paragraph>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                Was Sent To + (1) 555 678 2810
              </Paragraph>
            </View>
            <View style={styles.code_box}>
              <View style={theme.margin5}>
                <View
                  style={{ flexDirection: "row", justifyContent: "center" }}
                >
                  <TextInput
                    mode="outlined"
                    style={theme.textField}
                    autoCapitalize="none"
                    outlineColor="#707070"
                    activeOutlineColor={theme.palette.primary.main}
                    autoFocus={focus_input_field == "1" ? true : false}
                    keyboardType="decimal-pad"
                    onChangeText={(text) =>
                      this.onChangeHandler("focus_input_field", "2")
                    }
                    returnKeyType="next"
                    blurOnSubmit={false}
                    maxLength={1}
                  />
                  <TextInput
                    mode="outlined"
                    style={[theme.textField, { marginLeft: 6 }]}
                    autoCapitalize="none"
                    outlineColor="#707070"
                    autoFocus={focus_input_field == "2" ? true : false}
                    activeOutlineColor={theme.palette.primary.main}
                    keyboardType="decimal-pad"
                    onChangeText={(text) =>
                      this.onChangeHandler("focus_input_field", "3")
                    }
                  />
                  <TextInput
                    mode="outlined"
                    style={[theme.textField, { marginLeft: 6 }]}
                    autoCapitalize="none"
                    outlineColor="#707070"
                    autoFocus={focus_input_field == "3" ? true : false}
                    activeOutlineColor={theme.palette.primary.main}
                    keyboardType="decimal-pad"
                    onChangeText={(text) =>
                      this.onChangeHandler("focus_input_field", "4")
                    }
                  />
                  <TextInput
                    mode="outlined"
                    style={[theme.textField, { marginLeft: 6 }]}
                    autoCapitalize="none"
                    outlineColor="#707070"
                    autoFocus={focus_input_field == "4" ? true : false}
                    activeOutlineColor={theme.palette.primary.main}
                    keyboardType="decimal-pad"
                    onChangeText={(text) =>
                      this.onChangeHandler("focus_input_field", "5")
                    }
                  />
                  {focus_input_field == "5" ? (
                    <IconButton
                      icon="check"
                      color="green"
                      size={20}
                      style={{ marginTop: 20 }}
                      onPress={() => console.log("Pressed")}
                    />
                  ) : null}
                </View>
                <View style={theme.margin5}>
                  <Button
                    mode="contained"
                    uppercase={false}
                    contentStyle={[
                      theme.button_container,
                      { backgroundColor: theme.palette.primary.main },
                    ]}
                    style={theme.button_border_radius}
                    onPress={this.navigateToFingerprint}
                  >
                    Verify
                  </Button>
                </View>
              </View>
            </View>
            <TouchableRipple
              rippleColor="transparent"
              onPress={this.navigateToFingerprint}
            >
              <View style={theme.margin5}>
                <Paragraph
                  style={[
                    theme.secondary_title,
                    { textAlign: "center", color: theme.palette.primary.main },
                  ]}
                >
                  Re-Send Code In 0:59
                </Paragraph>
              </View>
            </TouchableRipple>
            <View style={theme.margin5}></View>
          </View>
        </ScrollView>
      </>
    );
  }
}

export default verification;
