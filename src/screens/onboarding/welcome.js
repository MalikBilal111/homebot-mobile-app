import React, { Component } from "react";
import {
  Title,
  Button,
  Paragraph,
} from "react-native-paper";
import { StyleSheet, View} from "react-native";
import theme from "../../styles/theme";
import WelcomeIcon from "../../assets/svg/welcomeicon";
class welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onChangeHandler = (name, value) => {
    this.setState({ [name]: value });
  };

  navigateToFingerprint = () => {
    this.props.navigation.navigate("DashboardScreen");
  };

  render() {
    const styles = StyleSheet.create({
      text_align_center: { textAlign: "center" },
    });
    return (
      <>
        {/* <ScrollView style={{ marginTop: 50 }}> */}
          <View style={[theme.container, theme.align_center]}>
            <View style={theme.align_center}>
              <WelcomeIcon/>
            </View>
            <View style={theme.margin5}>
              <Title style={[theme.mainTitle, styles.text_align_center]}>
                Welcome
              </Title>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                Well you have thus come this far!
              </Paragraph>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                Speak friend and enter or some lorem
              </Paragraph>
              <Paragraph
                style={[theme.secondary_title, styles.text_align_center]}
              >
                ipsum
              </Paragraph>
            </View>
            <View style={theme.margin5}>
              <Button
                mode="contained"
                uppercase={false}
                contentStyle={[
                  theme.button_container,
                  { backgroundColor: theme.palette.primary.main, width: 200 },
                ]}
                style={theme.button_border_radius}
                onPress={this.navigateToFingerprint}
              >
                Hooray!
              </Button>
            </View>
            <View style={theme.margin5}></View>
          </View>
        {/* </ScrollView> */}
      </>
    );
  }
}

export default welcome;
