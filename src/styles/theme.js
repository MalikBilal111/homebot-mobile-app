import { DefaultTheme } from "react-native-paper";
const primary_color="#7C68EE";
const secondary_color="#8C9AAC";
const font={
  typography: {
    fontFamily: [
      "Lato",
      "TTNorms",
      "Roboto",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
}
const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#006ab3",
    accent: "yellow",
  },
  typography: {
    fontFamily: [
      "Lato",
      "TTNorms",
      "Roboto",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
  container_margin:
  {
    marginLeft:20,
    marginRight:20,
  },
  container: {
    flex: 1,
    backgroundColor: "#F1F2F4",
    fontFamily: font.typography.fontFamily,
  },
  mainTitle: {
    fontSize: 30,
    fontWeight: "bold",
    color: primary_color,
  },
  secondary_title:
  {
    fontSize: 17,
    color:secondary_color,
  },
  align_center:
  {
    justifyContent: 'center',
    alignItems: 'center'
  },
  margin5: {
    marginTop: 5,
    marginBottom: 5,
  },
  textField: {
    height: 47,
    marginTop: 5,
    marginBottom: 5,
  },
  button_container:
  {
    height:50,
    // backgroundColor:primary_color,
  },
  button_border_radius:{
    borderRadius:8
  },

  mainDescription: {
    fontSize: 12,
    color: "#B0BED4",
  },
  linkbutton:
  {
    color:"#006AB3",
    textDecorationLine:"underline",
  },
  autoCompleteInputProps: {
    borderRadius: 3,
    borderWidth: 1,
    borderColor: "#707070",
    backgroundColor: "transparent",
    color: "black",
    paddingLeft: 18,
  },
  autoCompleteErrorInputProps: {
    borderRadius: 3,
    borderWidth: 2,
    borderColor: "#C42607",
    backgroundColor: "transparent",
    color: "black",
    paddingLeft: 18,
  },
  
  textBox:{
    height: 120,
    padding:10,
    textAlignVertical: 'top',
    marginTop: 5,
    // marginBottom: 5,
  },
  button: {
    height:46,
    flexDirection: "row",
    textAlign:"center"
  },


  cardSecondary: {
    color: "white",
    borderRadius: 8,
    width: "100%",
  },
  containerMargin: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    justifyContent: "center",
  },
  margin10: {
    marginTop: 10,
    // marginBottom: 10,
  },
  buttonRadius: {
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 12,
  },
  groupButton: {
    backgroundColor: "white",
    color: "black",
  },
  backButonMargin:
  {
    marginTop: 40,
    marginleft: 20,
  },
  Chevron: {
    backgroundColor: "transparent",
  },
  palette: {
    primary: {
      main: "#7C68EE",
      light: "#2680be",
      dark: "#005a98",
      contrastText: "#fff",
    },
    secondary: {
      grey:"#7B859A",
      main: "#FC443E",
      dark: "#EC3A34",
      light: "#FB5954",
      contrastText: "#fff",
    },
    
  },
};

export default theme;
