import React, { Component } from "react";
import Svg, {
  Defs,
  Pattern,
  Image,
  G,
  Path,
  Text,
  TSpan,
  Circle,
} from "react-native-svg";

class layoutdrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width={375}
      height={490.7}
    >
      <Defs>
        <Pattern
          id="b"
          preserveAspectRatio="xMidYMid slice"
          width="100%"
          height="100%"
          viewBox="0 0 200 200"
        >
          <Image
            width={200}
            height={200}
            xlinkHref="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCADIAMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD1BRThxTA1KDX4vF2PoyTrTSKclBrdO409SOlAp2KMUWHcQClApMGnChITYmPal4ApJHWNGd22qoJJPQAV4P8AF74zSWs02k+F5k3KSst32U+i4+8fXsK6sLhKmJnyU0S2krs9n17XNJ0S0a61bUbeyhA+9M4XP0HU/hXAax8cPAlihMNxe3zDtBbNj82xXzBd3Wo6rcvd39zLdTuSd8zE8/iT+VWbPRrmcHNvJuHXgBR+VfT4fh2mv4km35aHPLEW2Pb7z9onTiT/AGb4auZ8DP724VDx7AHNXfDHx30zVLtU1C1t9NhI5ZpWbB6+gzntivFU0OBVyZJARjooAH4nFZ50qCCaR1LSg/dwOOOx6iu2eQYRxtaz9SFiJ3vY+xfDvizw7r6j+ytXtLh+6K43fkcGtxTgjmvh8MkN4JI3aDa+VYEg8ehGDXrXgf4q65pTQw6xP/a1h8qtvI8+IHuG/ix6N+deTjOH50o89J3RpDERk7PQ+jY2qYGsjR9Us9Ss472xuI54JRlXX+R9CO4rUiYGvl5wcXZmkkTZ4ptK3Sm81N+hCHA0p6UxSSacTQncOonen54plKKAYhopWxRRYEZgqRBTMc08GueOm50McOKcDmmZ5p6VvF9CdgxS0u3ig1oO4lIKXFcV8ZvEk3hvwPdT2jbLy4It4COoZu49wK0pUnUmoR3YHlnx/wDiZPdXUvhXw9dGOBCVvblDy56FAR/CO/rXjen6Y17Mixo0z+pztA9TjtVyDTpXnSFwZZpjliTgHHUk+grstH0yKKJYUTdxlu289Mk9l9AK/QcFhYYamoR/4c4qknN6lbR9CtraNZBGtxL/AH+wx2H/ANatoWkYhV7uaKKINwq8A98e9OnljVxb22ZpBw3oB6cdB7dTVS8RUTdLGZpAR1OFB9Af8K7kwUbbFDVr21iUm1RTz9988fTqa5ctqmo3QjjPmKzAAKCqj8xXW2uhavrE6iLEUeQMIAP8efxrr5/DNv4SgjN1EZ55k3As3CcfzoWpMkzzw6VDa2MkV5HIZWAMI2qcsOoIPp2rPtIsxO8wdNpxvGBx9Of5V0Op28+oy/O8hjwdoPOAO2Kqtpdiluu9pIs8qwbK5985xmrUlaxCpvdm18KPGlz4W1lbe5cy6TcOBIAc7CeNwHYjv6ivp6yuEniWSNwysAVYdCDyCK+J9SglsmJHKdQR3H+Ir3z9nXxc+saLJo9zLunsQPLJ6mM9M/Q8V8nnmXpL20V6nVSnfRntKtkUpqGFsipc5r5Jot6DlFIaM0UrE2E5pVzSHrTx0oSBjTRRjmii4zOakGaBTsVgtTYcop60xSKcprSDQmSA0uM02lzWyYARXgf7RetLNrttpYbK2Sea4zxuYenrive5GCxsc9Aa+U/HF8mp+LtT1KQAwee8nPdF+VQfrivXyaj7TEXfQmbtEzdLiSFGuLkDz5VDP/sA8qo/DmtgXOyzMUTbZXGZG67R2A965FL6Z5TJIcsXLH3J6AD9K6GwgctFGVLSEhmz1LHpmvuFornKtXZG3o1lO5S1tU3TMeh5APcnHXFen+F/hhaXEkd3qpkupSBnfwFPoAOAK2vhV4QhttPju5ogZWGcnrzXqllYqkQwOnSlFSm7o3kowWpzWleE9MsYwlvaxjAA6elQeKvCNvq1gwliUsqkLx0rvILTdzjpT7i3BQgCulUna5h7RbHy1rfht9LmciLaV9u3/wBauC8QEJ5kUiBcgtnpyOpH86+utf8ADtvexN5sQJ7HHNeHfEnwDKgkktUzjkDHXHcf1FZTbjoapKS0Pny9nlhkaGQllJPB5x/kdK3/AIQ6y3hvxzZXQJ8iZxDKo6FW4z+FVNZ0maF2jmUgrkLnrx2qnpcbLLG6f6yJw6kexz/SuTFJTpOL2ZlFWkfbdsQVBHerIrP0SUXGnW8w/jjVvzANaNfnE1qdEtBKCaXFBFRsTcSlBppOKFJNK/QY85opCTRVaAZuadmmUorlTN2hy1IlNQU8CtIqxLY/Ap20YpvNLmtkxFTWJPK025k/uRsfyGa+SPEJCWzRBvmuHGf90c/qa+rPFsqw+Hb6Q9oH/wDQTivlW2tHv9SLnPlxOOvOAOtfTcPRvKTIqu0bGXplm7TxGTpvDH8Sev4Cu78Iiwm1m3k1C6jtIXkLkvwMDt+XFYV3CqE2sXDFwGxyQMZP866zw1pvhq8iR9YuZ4GUFVMUwUY+mDnNfXcqbsYQve59BeEvFPhULHbRa1ZNg7QvmgHIGehxXoVjdWVxErwSxyKehUgg/lXzTH8N/DV+El0zXZ1kkwUW4XOcdMHAz1r0j4dadfeH1FhNdefGD8hyePzrVLk2HKPPqesF409qytU8TeHtO3Lf6nbwSAZ2swzimXs7C0JyRxXkHiXwVHq2pSX95ftGCTjg8Dr61ftGnZGcKSerPSbzxj4VdW265Yn6S1h3d7pOq7ltry1ugRnEThiPfArhrPwj4LhRpbvW7qZkI3EHYg9uBirkPhnwrcuk2lX08Nwn+rlSYE5H86irZrU3pwscP8WvCa+U95ar33YFePWUGy9IIxzg/j0NfTuvWkkmlSwzP5hCH5sYyQOtfOs1uf7bh2nCyFgT7q39K82r8DuVUik00fVHgtQ/h2ybP/LBP/QRW3iue+GiuPBtiHJJVSvPXg10RBFfn9dWk7CluNOKTFKTzQSKwuIawoVcClxS0ra3GxpooaigZlqacDUQNSCuNM6GSqwp2aiBp2fetlIholVuaXPNRZ4oLADn86pSEkZvi6BrvQ7m0jxuljZVPvtJH8q+c9NiW1gmklwuzIb8Of619D+I50SwdhKwIU4x64+lfM/jHWIJL6a0tHBUMQWzkYHXkda+q4dqNOVgqxXKrkuj276nqGEP7yRsjvjNdR4f8Lxaf4qik8VwXk2l7DtWJflLEYBIGCQD2zzTvgdYpf6wspQlYzuye/GB/Ovpm00a0urZEkhVsDuM19dSk07mTpx5bs+fPh14C1ODXTPqGrP/AGOju0AtQEmfcMKCTzgEKQG6f8Cr2izivbGysJL2QSTglHcDG8A4ViOxI6+9dZbaFaWwDiJVx6CsPxAwkvI4x2YAD8a6JS92zJpqN7ROh1tcaXb7ODIvNee+OdKuTc20U88w04EGZYMiSQHnAOOB6+teg32Wt7UHoAK1rWztL6xVZo1bAxzTUbyI5+SOp8m6Z4Cul8WrLqer28/h0XTPIkcrLdyIcfKSCGwQOF6A11HgPwhqkWrXUsVzcDThITbicZfZngE9yPWvoCXwzpobcLdfrgVWubCG2UiNQABU1E7alUpQv7p514niNppkxY5wh/lXgXh7TW1OYNG37y38yQg8ZVgc+te9/E2bytJnx1KkCvH/AAAjQeMo7eXIjaIhv93PP4A815mJqctKbR1cuqZ7T8N0aLwlZxMpEiqQ4PUMDg5roXBNU9DRQk+z7vnHb+AAP61oMK/PpNy1ZjU+JlZ80hNOkHNNNcz0JTAHilBpmeKAeaFLUdhXNFIaKHIZkd6eGpmaM1x3sdLRKGpwaocmlDVSkKxYBGKq6reR6fp1zezZ8qCNpHAGSQoycAd6kDcVQ1tkksZoZVVkkRg4PTbjnPtWsWm1cFHU8P8AGfivxb4oQWmmQwafYTfL5gZWkkz1GRkDA646dzXGXPhsWtu8cTmaRzsDnqx6sw9q0xqVydUbSbORpIQTHGcYMcO77p9B6nrXbajaQW+mCz0+NbrUJQA06jKxr3UH1PrX3WFlHCw2SX9asycecg+B99Fbate24GDFIEPvgV9N6DeI8KkHsK+Z9H8NyeG/EOYSxR7eKSYt1V2ZlP4Er/49Xsvhe9lWFcscYFevhcTCvHngynH3eVnp8kgaE49K4gBrrxCFz8qtj8RW1bajmLGe1cRrOmeL4dZTUNCvbOeFJC5tJYirSgn7ofPyt6HGPWu6TvZmEI8t0ekaxEyW0THGNoIqz4fuSIiOwPNcgj+Ktbth5UUOjmM4kF0nmuT6BVYDB/vE102h+ZY6aI76WKW4bmRkUqvpgA54qot810ROPuWZ0MksZTqKwNcuFVG57VHdXYzmJuKw9YnleNgSairUurDo07O55/8AEK4a5Rogflzya5bT9Jmjkj1SJMGPLIw+XcmcE5PbJWvQ4olRLy5lBIWNgccnAUk4FOsFhv8AyZgpaA2yJgjkhhnBr5TG5i1KVNLRbnemjQ8NXUcthFghoyOo7HPIP41rtk8YrhvD8suk+JbzS5WPknDjPYHHzD655/3a7jJC49K+cm1d2OWsrSuiGQcmomqV+9RMa5JPUzQnakB5NB6UgrO5SDPIopO9FFxmQpyKd0qNDzUuRXOtTraEDDvSMwppPNNbOOtS2wSJVYVn65H5sJh3+Ws6MjP/AHRtJJ+tT7iKSZFniMbdDTjUsxpWZ8/+AdGF1f3On5AuPtBLBjt3gE4PPJJHQD8a9j0Kxt7SF7LydpkUgORyG9PbPX61zMHhpYfFc8jr5eVVwV46cEg+2F+ldY1yHkW2mf8A0iPDo4OPMUEAn6jPNfSYqvLEU+ZPQcIcpW1eCJp5ppeA+nFSe4ZJM/pmpfBmuW13ZRNHKpDgYIqv47uhpXhm61WZS0SRSKSf4WZV2g/UivCPhP4yNtqh0q7lCb2zbnPB9V+vcV7HDynyyf2dDGrJJ2Z9NeIrvXbCyXUNMtob2KPJkgL7JGHqGPy8eh61k2PjjxUxSWLw9LsBBxCFlP5Bsn8BWp4Z1WO8tVhkIZXGKn/4Ra9gvPtOkThMtkIwJAPsR0r6yNmdeAq4aEmq6v2LEPjbxBewA6f4bvIpcAy+dbsoBHfLYAFPs/Enii/cWp0exWVzje1xjaP7xVc8CrkOk+LbkeVc3ECREANtVhkdsjIrf0jQk02LP3pD95j1NWzfEYnAxptRgm+lr/izOtLe9sgBd3IuCRy4TYCfpk4FUtc1GKGJnkYAAVq+IbuOCI5IzXhnxh8UXVhbWptcHz7nZg9CoUk/hXDiqnLFqO55NPX3melpKslg6Ivms6ONo7kg/wCNSeElWLwxY7yc+QrMfVsYP8sCuX+CN3dah4OSa9RyTcOqMx+8oPb2HQVseFJZL7RY7WOSSKKFXhklQ4IYORtU/wB4Dqe1fn81K81N63/zNJNPYZgar40aeBSYrC3MEr/w72YHYT3IAzjtursFzsGeuOaqWFpa2NsltawrFEgwqjt3J9ye56mrQYVyNpswqS5tENcVEQc1MxFRGspK5CGNSZpTyaSsmNCZopMCikUYinmpA3FQg04GuZPsdthWbmmk0E00mqsUkOxmkoBpCRVKI0iG7jVgsowGiYsD7fxD8RXParGIr+CSP5ts6NGRz8j8Mv0x0ro55o44mLc9flxkn2ArOs7FnEEk2CYyGP1AwoGPQdfeu+hKUYXew7GJ8ZAsnw41m1EwjlKxvEmCfMKsQV9ABnPNfH9/5tvMWBKMrZBU8bh6emK+ufi3BcXPhyfyeiJuc+2elfNmt6UlwirEwyW2/Via+w4YrN0Gn0PNxsddD1/4eeNpbCa2h1JsBlUq575Hf3r6X8Ha1aXtqkiyKQQDnNfKN7oDTaeojTlUAH4CmeGvFnibwtN9nhlaSIHAV8kYr3adVw3Cyasz7jhurcoCGWsXxHrVpZwsXlQAD1r580f4oeJr4LHHZwqTxuLE1v2Vvq2tTLLqEzNk52jhRW0sTdWSFGhFO7ZsahqNxrl2xjBWDOB7ivNvjHol7e3ugWtmhJa5dSf4VJA+Y/SvZ9M0lbe3Hy9B6Vla1Zq11FI6ZKOGX2Oa8rHSnTpOcdzeLUvdLXhi1j07S7XT4hiOBFReAucdScdzWf4Mnazhk0u6gaBjcTSQMwwsqNIx4z/EO4/GteHLKuasSxRTJslQOPevz+NVNOMurCVr6ltOlKSax7Y3FvqiW0Vy08DKS8bjLQjHB3dwTwAea2ARik7XsjGUbCc0hFPbpTeKlokYBzQwqSmt1pctgTIiKKVjRWbKRz4oORRnFNZq40d6QjE00mkdhTSwq00WhwY1G+Sew5/Gms3NRtIfWkqlthoeqIH3kZNS7+Kql/ekadUXMjAD1NaKpOo1HcYmr263WnzwuMh0I/TivnPSfCWqzeMVN3aSwWdrIZG3gruYElVAPXnn6V9Gwpe3zbLWIxx95GH8h/jVG80gR3rRH5mznJ5JzX2nD+ExGHUpTVkzjxHLKyMzStLDwqCvUCrF34Kt7sq/kjP0rsLPSTHao4XoBWxpkQ24I6V9Ml0MLnJ+HfCFta7f3OMe1dtpemRx4woAHtWnbQx4zgVbjVQvArZRSIlUurFOaNVTAFY+pWnmoTit2RTJJsFJPbhYzkVE6amrMUZWZyMcnksI5jt7Bj0P/wBepxLI0pVFIRernv7D1+ta8unRTxMjqDkVky6Ve2jn7K3mR/8APN+R+B7V8lj8hkpOdD7jeMlL1JYY44gRGuMklj3JPcnualQkmqP2tIm2XKNA3+1yPzFW4ZUkGY2Vh7HP8q+aqUKtKVqiaZMovcnJ4oyKjJpNwqOfUzsSlgKYzCmE012HrUyqFJAWzRTAc0VlzXGc95lNaSo8kU1jXlqrI70Ejk1GrsDUdzcRQRNLM4VV6k1kNqc9zJstYvLj/vsMsfoO1d2DwGIxcv3a+fQG0tzbeQ4qrPdRRMqF8u6llVeSQDjPHvUdtpzXABuZZZPXJ4/Kq9pYiz+I2mwySyPp9/YzRrEWOxJo2D7gOgJUtn/dr6jB8K8z/fT+4yqVuWzRbVpX+8fKX1PJrS01dHjYSXMxkYd2BNdhHotiIOEVjjvVae0toScwr+VfS4TKKOE1pxXqZOtzaGXNrtjDEY7SKQkjAwhqnaBrq4EskUqEHIO05rpLOO3cgeUBWzaWlvjOK9ONOT1uZykkU7O4iktdkkRVscjHH1HtU+n2yFyTwDV17WFl+UYPYjtUlvb4GGGD7f0rbke7MU0hRaMFynIpyAdzTXuWtgyjnNQW6vIS8hwKbfRBa+rHSXcduxcIWNY97rN1K422MpiHTBALH157V0C2gZhlQRx+tWFsYFGSmaOSb0TEpQWrRyC6nqA/5h8nt8wqT+1NRcf8gyR/+BCunnitVHEfT2qsZCOEUCs3Skt2aKpF7I46+g1q8k4sI4wR1LZP8qpS6JdRrvkTy29Rx/KvQwyovmSv07Vzuu6gZnZYlwBXPVw0GrydzaFVvSxyH2+9sL6K2aYy+YkrKj8/cTPU8jkqKng16dFUXdoM45MTf0P+NYN0Z7v4mWtqGO2DSpZXH+/IoGf++a6b+yWYZK15eJyfD1F8Oo4uMm7k9tqtnckJHMFb+4/yn9at8msO60QkZ2Umn3VxYSrb3TFoCcBjyUP19K+ax2RTpRc6buuw5UlvE3gcUU0nNFfP8zWhlY5dmqNmppas7xBdm10maRDiRgET/ebiuLD0HVqRprq7Hc9NTJup31bVNikm3hYqnoT3b/Cur0nS1EY+WsDwnaAKvHpXoemwr5Y4r9ZwmEhh6SpwWiOVye7I7OwXbjFYfi62Flqvh263xR51VIcuOf3iOuB/tH+VdpEqr0rjviwC8HhsrnK+IrI8f7zV6NOKTszCcnY6+Gd4owCaqXdz5hwafOwYHFVdhZqXM3oaJdS1ZyKMZFakNwABsNZccZxUihlrVSsJxTOgtJQSOauSurJjODjqDXNx3LJVhL1iBmtFVRm6XU0yu91DDJJAyPfirAjVMphSCOCDnvg/yrLF2SvU59aWTUZGOSeck/mcn9aanFai5JPQ2o5FQYoe5UDrXPvfSE9aVbhj1p+26IXsu5ryXKEE4FULq4OSRUG8mmuCQazlNtFKFitc3DvkZNQRwBuSKnaLnpUmNqmue19zZuysjjtCthL8WtfmxlbbTbSAfVmZzXdLEuAMVyPgv5/FXi67Pe/igB9o4F/q1dcjZIrolbYwSaJDao6crXN6/YAbjtFdbGfkrJ1iMOjfSsq0FKJdObTOc0yQtb7XPzRnb+A6UVDafu76WP8AvAH8jRX5ZmlF0MTKK2NmtTAZua53xfKWaztgfvSFz+AwP51vO1cxrLCbxDDH/wA84x+ZOa04eoe0xsW+mp0VXaJ1HhmMLGvHpXb2HCCuU0CLCLXWWa/KK/SoI5HsWHk2jrXIePrlfM0CMjOdbtj9CNxrqbs7UJ9K43xULqfU9JFpdCER3QlmU8+agGCvt1z/AMBqk2pEyVo3OtgO9QanSLnOKraaSyrWtHGMCrhG427ECjFPADVM0QxSJGVrTlFcWO3U1OllnHFEQ5FaEABAqlBMiUmit9h2rSLZBq0nK7TkVX85QSM1XJFEqcmVfsC5oe2CVbEqjnNRyOGOKXLFbDUm9yqQAelIV7CpGQscU+OIip5bl3K/l1BdHapxWk8ftWdejBH1FRKNgi7nG+BJ9za9Pn/W63cnPrtKp/7LXY2sm415j8PdRH9hNKTzcajdyZ9d1w+P0Feh6XKGRTmob99odvdub8ZygqhqS5jb6VahPy1XvxlDWsldGcdGcXOfL1RP9okfnRSa0pS4WQfwsD+Ror4nN8DKtiOZLodsVdHNM3FctG3neKLon+Fwg/ACiiuHheK+sSfkVX+FHouhD5FrprbgCiivvInKxupMBbsfY15JqzXmoeMvD/kiRorG6lNwynhVKkLn2PSiiri/eJn8P3fmes6OAIlz6VsRYooransKRLtzSNGaKKtolPUfGpFWInC0UUkKQ+5kPlfhxWaJCWPNFFOYQRKjHNTICaKKlDZYSPNWFjwOlFFaJEN6iSKCOlYmtEpbyuvVVJA9wM0UVnUWhUGeNSQW/huDSdLtb0Xkce/zJx0eTJL4x2DuwH+7Xo3hy58yBCT1AoornfxXNYawR1Nu2VFJeDKGiitvsma3ON8QREBj7GiiivOqxTkdcXof/9k="
          />
        </Pattern>
      </Defs>
      <G data-name="Group 3259">
        <Path
          d="M337.5 0H35C15.7 0 0 14.056 0 31.335v397.95c0 18.532 16.8 33.573 37.5 33.573h300c18.5 0 33.9 12.086 37 27.843a24.736 24.736 0 0 0 .5-5.461V31.335C375 14.056 359.4 0 340 0Z"
          fill="#7c68ee"
        />
        <Text
          transform="translate(144 425)"
          fill="#fff"
          fontSize={18}
          fontFamily="Lato-Bold, Lato"
          fontWeight={700}
        >
          <TSpan x={0} y={0}>
            {"Dashboard"}
          </TSpan>
        </Text>
        <G filter="url(#a)">
          <Circle
            data-name="Ellipse 4951"
            cx={15}
            cy={15}
            r={15}
            transform="translate(20 402)"
            fill="url(#b)"
          />
        </G>
        <G filter="url(#c)">
          <Path
            data-name="filter"
            d="M341.336 424.167a1.078 1.078 0 0 1 0-2.15h7.334a1.078 1.078 0 0 1 0 2.15Zm-2.666-6.092a1.078 1.078 0 0 1 0-2.15h12.667a1.078 1.078 0 0 1 0 2.15Zm-2.667-6.091a1.04 1.04 0 0 1-1-1.076 1.04 1.04 0 0 1 1-1.075h18a1.04 1.04 0 0 1 1 1.075 1.041 1.041 0 0 1-1 1.076Z"
            fill="#fff"
          />
        </G>
      </G>
    </Svg>
    );
  }
}

export default layoutdrawer;
