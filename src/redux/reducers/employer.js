import * as ActionTypes from "../actionTypes";
import moment from "moment";
// TODO: we might also have to add checkout time

// checkinStatus is 1 for checked in 2 for checkout
export const employerInfo = (
  state = {
    balance: 0, card_brand: "",
    card_last_four: "",
    expiry_month: "", expiry_year: "", charge_amount: 0,
    payment_method: false, show_tab: true,
    isLoading: true, is_company_exist: false,
  },
  action
) => {
  switch (action.type) {
    case ActionTypes.SET_EMPLOYER_INFO:
      delete action.payload.client_secret
      delete action.payload.public_key
      return { ...state, ...action.payload, isLoading: false }
    case ActionTypes.CLEAR_EMPLOYER:
      return {
        balance: 0, card_brand: "",
        card_last_four: "",
        expiry_month: "", expiry_year: "", charge_amount: 0,
        payment_method: false, show_tab: true,
        isLoading: true, is_company_exist: false,
      }
    default:
      return state;
  }
};
