import * as ActionTypes from "../actionTypes";
import moment from "moment";
// TODO: we might also have to add checkout time
export const checkin = (
  state = { isCheckedin: false, checkinTime: moment() },
  action
) => {
  switch (action.type) {
    case ActionTypes.TOOGLE_CHECKIN:
      if (state.isCheckedin === null || state.isCheckedin === false)
        return { isCheckedin: true, checkinTime: moment() };
      else {
        return { isCheckedin: false };
      }
    default:
      return state;
  }
};
